function New-AzServiceBusSasToken {  
    param( 
           
        [Parameter(Mandatory = $true)]
        [string]
        $Namespace,
        [Parameter(Mandatory = $true)]
        [string]
        $PolicyName,
        [Parameter(Mandatory = $true)]
        [string]
        $Key
    )

    $origin = [DateTime]"1/1/1970 00:00" 
    $Expiry = (Get-Date).AddMinutes(5)    

    #compute the token expiration time.
    $diff = New-TimeSpan -Start $origin -End $Expiry 
    $tokenExpirationTime = [Convert]::ToInt32($diff.TotalSeconds)

    #Create a new instance of the HMACSHA256 class and set the key to UTF8 for the size of $Key
    $hmacsha = New-Object -TypeName System.Security.Cryptography.HMACSHA256
    $hmacsha.Key = [Text.Encoding]::UTF8.GetBytes($Key)

    #create the string that will be used when cumputing the hash
    $stringToSign = [Web.HttpUtility]::UrlEncode($Namespace) + "`n" + $tokenExpirationTime

    #Compute hash from the HMACSHA256 instance we created above using the size of the UTF8 string above.
    $hash = $hmacsha.ComputeHash([Text.Encoding]::UTF8.GetBytes($stringToSign))
    #Convert the hash to base 64 string
    $signature = [Convert]::ToBase64String($hash)

    #create the token
    $token = [string]::Format([Globalization.CultureInfo]::InvariantCulture, `
            "SharedAccessSignature sr={0}&sig={1}&se={2}&skn={3}", `
            [Web.HttpUtility]::UrlEncode($Namespace), `
            [Web.HttpUtility]::UrlEncode($signature), `
            $tokenExpirationTime, $PolicyName) 
    return $token
}
function Send-AzServiceBusMessage {
    param(
        [Parameter(Mandatory = $true)]
        [string]
        $ResourceGroupName,
        [Parameter(Mandatory = $true)]
        [string]
        $NamespaceName,
        [Parameter(Mandatory = $true)]
        [string]
        $QueueName,    
        [Parameter(Mandatory = $false)]
        [string]
        $PolicyName = 'RootManageSharedAccessKey',
		[Parameter(Mandatory = $true)]
        [string]
        $Message,
		[Parameter(Mandatory = $true)]
        [string]
        $Description,
		[Parameter(Mandatory = $false)]
        [string]
        $StackTrace,
		[Parameter(Mandatory = $true)]
        [string]
        $TimeStamp,
		[Parameter(Mandatory = $false)]
        [int]
        $Priority = 4,
		[Parameter(Mandatory = $false)]
        [bool]
        $IsError = $false,
		[Parameter(Mandatory = $true)]
        [int]
        $MonitorType,
		[Parameter(Mandatory = $true)]
        [int]
        $MonitorSource
    )
      
	$logMessage = New-Object PSObject -Property `
              @{m = $Message; ds = $Description; st = $StackTrace; `
              dt = $TimeStamp; p = $Priority; er = $IsError; mt = $MonitorType; `
              ms = $MonitorSource }
	$jsonMessage = $logMessage | ConvertTo-Json 
	  
    $messageObj = [PSCustomObject] @{ "Body" = $jsonMessage; }
    
    $Namespace = (Get-AzServiceBusNamespace -ResourceGroupName $ResourceGroupName -Name $namespacename).Name
    $key = (Get-AzServiceBusKey -ResourceGroupName $ResourceGroupName -Namespace $namespacename -Name $PolicyName).PrimaryKey
    
    $body = $messageObj.Body 
    $messageObj.psobject.properties.Remove("Body")

    $token = New-AzServiceBusSasToken -Namespace $Namespace -Policy $PolicyName -Key $Key
    
    #set up the parameters for the Invoke-WebRequest
    $headers = @{ "Authorization" = "$token"; "Content-Type" = "application/atom+xml;type=entry;charset=utf-8" }
    $uri = "https://$Namespace.servicebus.windows.net/$QueueName/messages"
    $headers.Add("BrokerProperties", $(ConvertTo-Json -InputObject $messageObj -Compress))

    #Invoke-WebRequest call.
    #The normal output of the command is redirected to the $null automatic variable.
    #If an error occurs, Invoke-WebRequest will output the error to the error stream stderr.
    Invoke-WebRequest -Uri $uri -UseBasicParsing -Headers $headers -Method Post -Body $body > $null
}

function Azure-Connect{
    $connectionName = "AzureRunAsConnection"
    try
    {
        # Get the connection "AzureRunAsConnection "
        $servicePrincipalConnection = Get-AutomationConnection -Name $connectionName

        $connectionResult =  Connect-AzAccount -Tenant $servicePrincipalConnection.TenantID `
                                -ApplicationId $servicePrincipalConnection.ApplicationID   `
                                -CertificateThumbprint $servicePrincipalConnection.CertificateThumbprint `
                                -ServicePrincipal
    }
    catch {
        if (!$servicePrincipalConnection)
        {
            $ErrorMessage = "Connection $connectionName not found."
            throw $ErrorMessage
        } else{
            Write-Error -Message $_.Exception
            throw $_.Exception
        }
    }
}

Export-ModuleMember -Function Send-AzServiceBusMessage
Export-ModuleMember -Function Azure-Connect