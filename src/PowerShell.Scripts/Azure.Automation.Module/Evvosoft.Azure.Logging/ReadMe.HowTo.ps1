# NOTE: If you need to change behaviour please bump ModuleVersion in Evvosoft.Azure.Logging.psd1 file
# Make Evvosoft.Azure.Logging.zip using Evvosoft.Azure.Logging.psd1 and Evvosoft.Azure.Logging.psm1
# Upload PS module Evvosoft.Azure.Logging.zip to desired destination

# build message to send
$message = 'I am message'
$description = 'Huge description here'
$stackTrace = 'Stack trace and more details over here'
$dt = Get-Date -Format "o"
$priority = 4 #4 low, 3 medium, 2 major, 1 blocker
$isError = $false
$monitorType = 1 # 1 general
$monitorSource = 34 # 34 AzureRunbooks

# service bus parameters
$ResourceGroup = 'Env_Resources-DEV'
$NameSpaceName = "ServiceBusMonitorEnv"
$QueueName = "monitorlogenvqueue"

# open azure connection only once
Azure-Connect

# sending message to service bus
Send-AzServiceBusMessage -ResourceGroupName $ResourceGroup `
    -NamespaceName $NameSpaceName `
    -QueueName $QueueName `
    -Message $message `
    -Description $description `
    -StackTrace $stackTrace `
    -TimeStamp $dt `
    -Priority $priority `
    -IsError $isError `
    -MonitorType $monitorType `
    -MonitorSource $monitorSource

# another sample message with less parameters
Send-AzServiceBusMessage -ResourceGroupName $ResourceGroup `
    -NamespaceName $NameSpaceName `
    -QueueName $QueueName `
    -Message $message `
    -Description $description `
    -TimeStamp $dt `
    -MonitorType $monitorType `
    -MonitorSource $monitorSource