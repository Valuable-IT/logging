using System.IO;
using System.Threading.Tasks;

namespace Evvosoft.Logging.Azure.Shared.Model.Logging
{
    public interface IAttachmentUploader
    {
        Task<MonitorAttachmentDto> UploadAsync(Stream stream, string blobFileName);
    }
}
