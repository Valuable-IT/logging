using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Blob;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Evvosoft.Logging.Azure.Shared.Model.Logging
{
    public class AttachmentUploader : IAttachmentUploader
    {
        private const string AttachmentsContainer = "attachments";
        private readonly string connectionString;

        public AttachmentUploader(IConfiguration configuration)
        {
            connectionString = configuration.GetConnectionString("EligibilityBlob");
        }

        public async Task<MonitorAttachmentDto> UploadAsync(Stream stream, string blobFileName)
        {
            try
            {
                string containerName = $"{AttachmentsContainer}/{DateTime.Now.Year}/{DateTime.Now.Month}/{DateTime.Now.Day}".ToLower();

                var azureStorageAccount = CloudStorageAccount.Parse(connectionString);
                var blobClient = azureStorageAccount.CreateCloudBlobClient();
                var blobContainer = blobClient.GetContainerReference(containerName);

                var newBlob = blobContainer.GetBlockBlobReference(blobFileName);
                if (await newBlob.ExistsAsync())
                    throw new Exception($"Blob '{blobFileName}' already exists in container '{containerName}'");

                stream.Position = 0;
                await newBlob.UploadFromStreamAsync(stream);

                return new MonitorAttachmentDto { AttachmentData = newBlob.Uri.ToString(), AttachmentType = AttachmentTypeDto.Blob };
            }catch
            {
                return new MonitorAttachmentDto { AttachmentData = "Blob storage failed", AttachmentType = AttachmentTypeDto.Undefined };
            }
            
        }
    }
}
