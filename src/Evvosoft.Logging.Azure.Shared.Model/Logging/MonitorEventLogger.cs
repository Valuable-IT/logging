using System;
using System.Threading.Tasks;
using Evvosoft.Logging.Azure.Shared.Model.NLogExtensions;
using NLog;

namespace Evvosoft.Logging.Azure.Shared.Model.Logging
{
    public class MonitorEventLogger : IEventLogger
    {
        protected readonly Type _wrapperType = typeof(MonitorEventLogger);
        protected readonly ILogger _log;
        const int MaximumMessageSizeBytes = 250000;

        public MonitorEventLogger()
        {
            _log = LogManager.GetCurrentClassLogger();
        }

        public MonitorEventLogger(Type type)
        {
            _log = LogManager.GetLogger(type.FullName);
        }

        public MonitorEventLogger(string name)
        {
            _log = LogManager.GetLogger(name);
        }

        public MonitorEventLogger(ILogger log)
        {
            _log = log;
        }

        public bool IsTraceEnabled => _log.IsTraceEnabled;

        public bool IsDebugEnabled => _log.IsDebugEnabled;

        public bool IsInfoEnabled => _log.IsInfoEnabled;

        public bool IsWarnEnabled => _log.IsWarnEnabled;

        public bool IsErrorEnabled => _log.IsErrorEnabled;

        public bool IsFatalEnabled => _log.IsFatalEnabled;

        public void Trace(string format, params object[] args)
        {
            Log(LogLevel.Trace, format, args);
        }

        public void Trace(Exception ex, string format, params object[] args)
        {
            Log(LogLevel.Trace, format, args, ex);
        }

        public void Debug(string format, params object[] args)
        {
            Log(LogLevel.Debug, format, args);
        }

        public void Debug(Exception ex, string format, params object[] args)
        {
            Log(LogLevel.Debug, format, args, ex);
        }

        public void Info(string format, params object[] args)
        {
            Log(LogLevel.Info, format, args);
        }

        public void Info(Exception ex, string format, params object[] args)
        {
            Log(LogLevel.Info, format, args, ex);
        }

        public void Warn(Exception ex)
        {
            Log(LogLevel.Warn, null, null, ex);
        }

        public void Warn(string format, params object[] args)
        {
            Log(LogLevel.Warn, format, args);
        }

        public void Warn(Exception ex, string format, params object[] args)
        {
            Log(LogLevel.Warn, format, args, ex);
        }

        public void Error(string format, params object[] args)
        {
            Log(LogLevel.Error, format, args);
        }

        public void Error(Exception ex)
        {
            Log(LogLevel.Error, null, null, ex);
        }

        public void Error(Exception ex, string format, params object[] args)
        {
            Log(LogLevel.Error, format, args, ex);
        }

        public void Fatal(Exception ex)
        {
            Log(LogLevel.Fatal, null, null, ex);
        }

        public void Fatal(string format, params object[] args)
        {
            Log(LogLevel.Fatal, format, args);
        }

        public void Fatal(Exception ex, string format, params object[] args)
        {
            Log(LogLevel.Fatal, format, args, ex);
        }

        private void Log(LogLevel level, string format, object[] args)
        {
            _log.Log(_wrapperType, new LogEventInfo(level, _log.Name, null, format, args));
        }

        private void Log(LogLevel level, string format, object[] args, Exception ex)
        {
            _log.Log(_wrapperType, new LogEventInfo(level, _log.Name, null, format, args, ex));
        }

        public virtual Task Log(MonitorMessageDto message, bool isError)
        {
            CheckMessageValid(message, isError);
            _log.Log(message, isError);
            return Task.FromResult(0);
        }


        public virtual Task Error(MonitorMessageDto message)
        {
            CheckMessageValid(message, true);
            _log.Log(message, true);
            return Task.FromResult(0);
        }

        public virtual Task Info(MonitorMessageDto message)
        {
            CheckMessageValid(message, false);
            _log.Log(message, false);
            return Task.FromResult(0);
        }

        private void CheckMessageValid(MonitorMessageDto message, bool isError)
        {
            if (
                  (string.IsNullOrEmpty(message.Description)?0: message.Description.Length)
                + (string.IsNullOrEmpty(message.Message)?0:     message.Message.Length)
                + (string.IsNullOrEmpty(message.StackTrace)?0:  message.StackTrace.Length)
                > MaximumMessageSizeBytes / 2)// assuming unicode 2byte for character
            {
                _log.Log(message,isError); // log message before truncate

                // truncate string properties of message
                if (!string.IsNullOrEmpty(message.Description))
                    message.Description = "Original message was truncated because do not fit " + MaximumMessageSizeBytes + " bytes.\n" + message.Description.Substring(0, Math.Min(8000, message.Description.Length));

                if (!string.IsNullOrEmpty(message.StackTrace))
                    message.StackTrace = message.StackTrace.Substring(0, Math.Min(8000, message.StackTrace.Length));

                if (!string.IsNullOrEmpty(message.Message))
                    message.Message = message.Message.Substring(0, Math.Min(1000, message.Message.Length));

                _log.Log(new MonitorMessageDto { Message = "Fail to write to log, original message was truncated", IsError = true, Priority = MonitorPriority.Major },true);
            }
        }
    }
}
