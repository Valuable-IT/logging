using System;
using Newtonsoft.Json;

namespace Evvosoft.Logging.Azure.Shared.Model.Logging
{
    /// <summary>
    /// POCO object which describes the Message which is passed from monitored application
    /// </summary>
    public class MonitorMessageDto
    {
        // This const is used for forming custom log properties to allow log formatting
        public const string MessageLogProperty = "message";
        public const string DescriptionLogProperty = "description";
        public const string StackTraceLogProperty = "trace";
        public const string DateLogProperty = "date";
        public const string PriorityLogProperty = "priority";
        public const string TypeIdLogProperty = "typeId";
        public const string SourceIdLogProperty = "sourceId";
        public const string IsErrorLogProperty = "isError";


        public MonitorMessageDto()
        {

        }


        public MonitorMessageDto(MonitorSource source, MonitorType type, MonitorPriority priority, string message)
        {
            this.MonitorSource = source;
            this.MonitorType = type;
            this.Priority = priority;
            this.Message =  message;
            
        }

        public MonitorMessageDto(MonitorSource source, MonitorType type, MonitorPriority priority, string message, string entityName, string description,string stackTrace , MonitorAttachmentDto[] attachments=null)
        {
            this.MonitorSource = source;
            this.MonitorType = type;
            this.Priority = priority;
            this.Message = message;
            this.Description = description;
            this.StackTrace = stackTrace;
            this.Attachments = attachments;
        }

        /// <summary>
        /// Message payload which is mapped to Monitor.Name filed
        /// </summary>
        [JsonProperty(PropertyName = "m", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Message { get; set; }

        /// <summary>
        /// Detailed description of the message, any details if available
        /// </summary>
        [JsonProperty(PropertyName = "ds", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Description { get; set; }

        /// <summary>
        /// Stack trace payload if any
        /// </summary>
        [JsonProperty(PropertyName = "st", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string StackTrace { get; set; }

        /// <summary>
        /// Message timestamp, is used for messages ordering despite possible collisions
        /// </summary>
        [JsonProperty(PropertyName = "dt")]
        public DateTime Date { get; set; } = DateTime.UtcNow;

        /// <summary>
        /// Logging priority, message severity level
        /// </summary>
        [JsonProperty(PropertyName = "p")]
        public MonitorPriority Priority { get; set; }

        /// <summary>
        /// Monitor type, identified source of the message
        /// </summary>
        [JsonProperty(PropertyName = "mt")]
        public MonitorType MonitorType { get; set; }

        /// <summary>
        /// Monitor source, additional identified source of the message (legacy)
        /// </summary>
        [JsonProperty(PropertyName = "ms")]
        public MonitorSource MonitorSource { get; set; }

        [JsonProperty(PropertyName = "er", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool IsError { get; internal set; }

        public void SetError(bool isError)
        {
            IsError = isError;
        }

        /// <summary>
        /// Monitor message additional attachments (links to some additional payload)
        /// </summary>
        [JsonProperty(PropertyName = "att", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public MonitorAttachmentDto[] Attachments { get; set; }

        public override string ToString()
        {
            if (!string.IsNullOrEmpty(Description))
                return $"{Message}; {Description}";
            return $"{Message}";
        }
    }
}
