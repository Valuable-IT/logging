namespace Evvosoft.Logging.Azure.Shared.Model.Logging.Factory
{
    public static class LogFactoryInitializer
    {
        public static void Init()
        {
            LogFactory.Init(
                type => new MonitorEventLogger(type),
                name => new MonitorEventLogger(name));
        }
    }
}
