using System;
using System.Threading.Tasks;

namespace Evvosoft.Logging.Azure.Shared.Model.Logging
{
    public interface IEventLogger
    {
        bool IsTraceEnabled { get; }
        bool IsDebugEnabled { get; }
        bool IsInfoEnabled { get; }
        bool IsWarnEnabled { get; }
        bool IsErrorEnabled { get; }
        bool IsFatalEnabled { get; }

        Task Log(MonitorMessageDto message, bool isError);
        Task Error(MonitorMessageDto message);
        Task Info(MonitorMessageDto message);


        void Trace(string format, params object[] args);
        void Trace(Exception ex, string format, params object[] args);
        void Debug(string format, params object[] args);
        void Debug(Exception ex, string format, params object[] args);
        void Info(string format, params object[] args);
        void Info(Exception ex, string format, params object[] args);
        void Warn(Exception ex);
        void Warn(string format, params object[] args);
        void Warn(Exception ex, string format, params object[] args);
        void Error(string format, params object[] args);
        void Error(Exception ex);
        void Error(Exception ex, string format, params object[] args);
        void Fatal(Exception ex);
        void Fatal(string format, params object[] args);
        void Fatal(Exception ex, string format, params object[] args);
    }
}
