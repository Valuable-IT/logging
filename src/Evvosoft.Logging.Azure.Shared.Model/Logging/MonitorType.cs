namespace Evvosoft.Logging.Azure.Shared.Model.Logging
{
    public enum MonitorType
    {
        General = 1,
        Switch = 2,
        PO = 3,
        POAck = 4,
        EDIInvoice = 5,
        PriceFile = 6,
        Members = 7,
        EMR = 8,
        FDB = 9,
        EDI = 10,
        _340BInvoice = 11,
        CoPayInvoice = 12,
        _340BChainInvoice = 14,
        WelldyneMemberExport = 15,
        TrueUp = 16,
        SafewayMemberExport = 17,
        SafewayProviderExport = 18,
        Claim = 19,
        InvoicedReportMacrohelix = 20,
        SwitchPharmacyList = 21,
        SwitchAllDrugList = 22,
        SwitchPrescriberList = 23,
        Providers = 24,
        RetailClaims = 25,
        SafewayClaims = 26,
        UnknownClaims = 27,
        ReprocessClaims = 28,
        Database = 29,
        CapturedClaimExportFile = 30,
        PharmacyInvoice = 31,
        AdministrativeInvoice = 32,
        QuickBooks = 33,
        WelldyneProviderExport = 34,
        Monitor = 35,
        FileSyncInfo = 36,
        InvoiceChangeFormat = 37,
        ImportInfo = 38,
        PharmacyInactivation = 39,
        GoogleAPI = 40,
        SyncServiceMessage = 41,
        ProCareMemberExport = 42,
        ACH = 43,
        _340BCollectionInvoice = 44,
        ProCareProviderExport = 45,
        Maintenance = 46,
        QuickBooksInvoicesExport = 47,
        Eligibility = 48,
        SSIS = 49,
        AlmostEligibleClaims = 50,
        HL7 = 51,
        FTP = 52,
        CentralApi = 53,
        SQLAgent = 54,
        AzureScheduler = 55,
        InvoiceGenerationJob = 56,
        ReportsAction = 57,
        InvoiceGenerationMonitoringJob = 58,
        LogArchivation = 59,
        Central = 60,
        MonitorArchivation = 61,
        AuditRecord = 62,
        RectifyFileSunRX = 63,
        MedispanUpgrade = 64,
        AuthenticationMessage = 65,
        _340BDirectClaimImportStatus = 66,
        WelldynePriceFileExport = 67,
        ProCarePriceFileExport = 68,
        EligibilityDBMonitor = 69,
        RemediationFileCaptureRx = 70,
        MemberSlideExpiration = 71,
        _340BDirectStatement = 73,
        ReplenishmentFileCaptureRx = 77,
        SBAccountTypes = 78,
        SBAccumulations = 79,
        SBFacilityADTEventTypes = 80,
        SBCEFacilityLocations = 81,
        SBChargeDrugMasters = 82,
        SBChargeHistory = 83,
        SBConsildatedOutboundFileTypes = 84,
        SBCoveredEntities = 85,
        SBCrosswalks = 86,
        SBCrosswalkTypes = 87,
        SBDashboard = 88,
        SBDrugPrices = 89,
        SBDrugs = 90,
        SBEligibleServiceAreas = 91,
        SBExclusiveDrugs = 92,
        SBFacilityLocationTypes = 93,
        SBFacilityOrderConfigurations = 94,
        SBMedicaidPayors = 95,
        SBOrderBasedOnTypes = 96,
        SBOrderSetUps = 97,
        SBPatient = 98,
        SBPatientTypes = 99,
        SBPayerTypes = 100,
        SBPurchaseOrders = 101,
        SBWholesalers = 102,
        SBReports = 103,
        OregonClaimFileExport = 104
    }
}
