namespace Evvosoft.Logging.Azure.Shared.Model.Logging
{
    public enum AttachmentTypeDto
    {
        Undefined = 0,
        Url = 1,
        Blob = 2,
        WebLink = 3
    }
}
