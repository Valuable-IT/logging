namespace Evvosoft.Logging.Azure.Shared.Model.Logging
{
    public enum MonitorPriority
    {
        Undefined = 0,
        Blocker = 1,
        Major = 2,
        Medium = 3,
        Low = 4
    }
}
