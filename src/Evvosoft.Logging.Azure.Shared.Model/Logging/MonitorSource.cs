namespace Evvosoft.Logging.Azure.Shared.Model.Logging
{
    public enum MonitorSource
    {
        Central = 1,
        Import = 2,
        FileSync = 3,
        SyncService = 4, //BCD-8  
        Monitor = 5,
        SyncServiceNG = 6,
        NotificationSender = 8,
        OMS = 9,
        EligibilityCore = 10,
        EligibilityAPI = 11,
        CentralApi = 12,
        ServicesMonitor = 13,
        HL7 = 14,
        FTP = 15,
        ReportsSSISSync = 16,
        SQLAgent = 17,
        ReportsPortal = 18,
        AzureScheduler = 19,
        MonitorUI = 20,
        AuthServer = 21,
        RealtimeEligibility = 22,
        AzureAutomation = 23,
        SplitBillngAPI = 24,
        BackgroundJobs = 30
    }
}
