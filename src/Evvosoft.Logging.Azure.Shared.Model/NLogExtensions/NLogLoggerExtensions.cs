using System;
using Evvosoft.Logging.Azure.Shared.Model.Logging;
using Evvosoft.Logging.Azure.Shared.Model.Mapping;
using NLog;

namespace Evvosoft.Logging.Azure.Shared.Model.NLogExtensions
{
    public static class NLogLoggerExtensions
    {
        private static readonly Type DefaultWrapperType = typeof(NLogLoggerExtensions);

        public static void Log(this ILogger logger, MonitorMessageDto message, bool isError)
        {
            Log(logger, message, DefaultWrapperType, isError);
        }

        public static void Log(this ILogger logger, MonitorMessageDto message, Type wrapper, bool isError)
        {
            if (null == message)
                return;

            message.SetError(isError);// set error property directly to allow targets serialize it

            var log = new LogEventInfo(LogLevelMapping.MapLevel(message.Priority), logger.Name, message.ToString())
            {
                TimeStamp = message.Date,
                Parameters = new object[] {message}
            };

            // example: to use in config, ${event-properties:item=description}
            log.Properties[MonitorMessageDto.MessageLogProperty] = message.Message;
            log.Properties[MonitorMessageDto.DescriptionLogProperty] = message.Description;
            log.Properties[MonitorMessageDto.StackTraceLogProperty] = message.StackTrace;
            log.Properties[MonitorMessageDto.DateLogProperty] = message.Date;
            log.Properties[MonitorMessageDto.PriorityLogProperty] = message.Priority;
            log.Properties[MonitorMessageDto.TypeIdLogProperty] = message.MonitorType;
            log.Properties[MonitorMessageDto.SourceIdLogProperty] = message.MonitorSource;
            log.Properties[MonitorMessageDto.IsErrorLogProperty] = isError;

            logger.Log(wrapper, log);
        }
    }
}
