using System;
using Evvosoft.Logging.Azure.Shared.Model.Logging;
using NLog;

namespace Evvosoft.Logging.Azure.Shared.Model.Mapping
{
    public static class LogLevelMapping
    {
        /// <summary>
        /// Maps Monitor priority to NLog log level
        /// </summary>
        /// <param name="priority">Monitor priority level</param>
        /// <returns>NLog log level</returns>
        public static LogLevel MapLevel(MonitorPriority priority)
        {
            switch (priority)
            {
                case MonitorPriority.Undefined:
                    return LogLevel.Off;
                case MonitorPriority.Blocker:
                    return LogLevel.Fatal;
                case MonitorPriority.Major:
                    return LogLevel.Error;
                case MonitorPriority.Medium:
                    return LogLevel.Warn;
                case MonitorPriority.Low:
                    return LogLevel.Info;
                default:
                    throw new ArgumentOutOfRangeException(nameof(priority), priority,
                        "Monitor priority can't be mapped to NLog LogLevel");
            }
        }

        /// <summary>
        /// Maps NLog log level to Monitor priority
        /// </summary>
        /// <param name="level">NLog log level</param>
        /// <returns>Monitor priority level</returns>
        public static MonitorPriority MapLevel(LogLevel level)
        {
            if (level == LogLevel.Off)
                return MonitorPriority.Undefined;

            if (level == LogLevel.Fatal)
                return MonitorPriority.Blocker;

            if (level == LogLevel.Error)
                return MonitorPriority.Major;

            if (level == LogLevel.Warn)
                return MonitorPriority.Medium;

            if (level == LogLevel.Info)
                return MonitorPriority.Low;

            if (level == LogLevel.Debug)
                return MonitorPriority.Low;

            if (level == LogLevel.Trace)
                return MonitorPriority.Low;

            // Impossible case since LogLevel is class
            throw new ArgumentOutOfRangeException(nameof(level), level, "LogLevel can't be mapped to Monitor priority");
        }
    }
}
