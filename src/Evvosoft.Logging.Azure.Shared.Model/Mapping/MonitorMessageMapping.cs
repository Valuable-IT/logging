using Evvosoft.Logging.Azure.Shared.Model.Logging;
using NLog.Common;
using NLog.LayoutRenderers;
using System;
using System.Linq;

namespace Evvosoft.Logging.Azure.Shared.Model.Mapping
{
    public static class MonitorMessageMapping
    {
        private const string ExceptionFormatStr = @"toString";

        private const MonitorSource MonitorSourceId = MonitorSource.Central;
        private const MonitorType MonitorTypeId = MonitorType.General;

        public static MonitorMessageDto Map(AsyncLogEventInfo logInfo)
        {
            if (logInfo == null)
                throw new ArgumentNullException(nameof(logInfo));

            MonitorMessageDto message;
            var parameters = logInfo.LogEvent?.Parameters;
            if (parameters != null && parameters.Length > 0 && parameters.First() is MonitorMessageDto)
            {
                message = parameters.First() as MonitorMessageDto;
            }
            else
            {
                if (logInfo.LogEvent == null)
                {
                    throw new ArgumentException(nameof(logInfo.LogEvent));
                }

                var lEv = logInfo.LogEvent;

                string details = null;

                if (null != lEv.Exception)
                {
                    var exRender = new ExceptionLayoutRenderer { Format = ExceptionFormatStr };
                    details = exRender.Render(lEv);
                }

                message = new MonitorMessageDto
                {
                    Message = lEv.FormattedMessage,
                    Description = details,
                    StackTrace = lEv.Exception?.StackTrace,
                    Date = lEv.TimeStamp,
                    Priority = LogLevelMapping.MapLevel(lEv.Level),
                    MonitorSource = MonitorSourceId,
                    MonitorType = MonitorTypeId
                };
            }

            return message;
        }
    }
}
