using Evvosoft.Logging.Azure.Shared.Model.Logging;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Evvosoft.Logging.DataLayer.Interface
{
    public interface IMonitorEventStorage
    {
        /// <summary>
        /// Adds new monitor message records to the DB
        /// </summary>
        /// <param name="msgSet">Message set which should be processed</param>
        /// <param name="ct">Cancellation token to cancel the operation</param>
        Task InsertMessages(IList<MonitorMessageDto> msgSet, CancellationToken ct);
    }
}
