using Microsoft.Azure.ServiceBus;
using NLog.Common;

namespace Evvosoft.Logging.Azure.Classic.NLog.ServiceBus.Interfaces.Mapping.Strategies
{
    public interface IMessageMappingStrategy
    {
        bool CanMap(AsyncLogEventInfo logInfo);

        Message Map(AsyncLogEventInfo logInfo);
    }
}
