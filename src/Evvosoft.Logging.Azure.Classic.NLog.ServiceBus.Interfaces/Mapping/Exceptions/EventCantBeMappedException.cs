using System;

namespace Evvosoft.Logging.Azure.Classic.NLog.ServiceBus.Interfaces.Mapping.Exceptions
{
    public class EventCantBeMappedException : Exception
    {
        public EventCantBeMappedException(string message) : base(message)
        {
        }
    }
}
