using System;

namespace Evvosoft.Logging.Azure.NLog.ServiceBus.Interfaces.Mapping.Exceptions
{
    public class EventCantBeMappedException : Exception
    {
        public EventCantBeMappedException(string message) : base(message)
        {
        }
    }
}
