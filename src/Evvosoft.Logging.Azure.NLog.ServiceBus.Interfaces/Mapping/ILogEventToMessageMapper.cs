using Microsoft.Azure.ServiceBus;
using NLog.Common;

namespace Evvosoft.Logging.Azure.NLog.ServiceBus.Interfaces.Mapping
{
    public interface ILogEventToMessageMapper
    {
        Message Map(AsyncLogEventInfo logInfo);
    }
}
