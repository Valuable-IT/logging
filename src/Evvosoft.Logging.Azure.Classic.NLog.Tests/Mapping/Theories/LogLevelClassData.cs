using System.Collections;
using System.Collections.Generic;
using Evvosoft.Logging.Azure.Classic.Shared.Model.Monitor.Priority;
using NLog;

namespace Evvosoft.Logging.Azure.Classic.NLog.Tests.Mapping.Theories
{
    public class PriorityToLogLevelClassData : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[] {MonitorPriorityType.Undefined, LogLevel.Off};
            yield return new object[] {MonitorPriorityType.Blocker, LogLevel.Fatal};
            yield return new object[] {MonitorPriorityType.Major, LogLevel.Error};
            yield return new object[] {MonitorPriorityType.Medium, LogLevel.Warn};
            yield return new object[] {MonitorPriorityType.Low, LogLevel.Info};
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }

    public class LogLevelToPriorityClassData : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[] {LogLevel.Off, MonitorPriorityType.Undefined};
            yield return new object[] {LogLevel.Fatal, MonitorPriorityType.Blocker};
            yield return new object[] {LogLevel.Error, MonitorPriorityType.Major};
            yield return new object[] {LogLevel.Warn, MonitorPriorityType.Medium};
            yield return new object[] {LogLevel.Info, MonitorPriorityType.Low};
            yield return new object[] {LogLevel.Debug, MonitorPriorityType.Low};
            yield return new object[] {LogLevel.Trace, MonitorPriorityType.Low};
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
