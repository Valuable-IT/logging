using Evvosoft.Logging.Azure.Classic.NLog.ServiceBus.Interfaces.Mapping.Strategies;
using Evvosoft.Logging.Azure.Classic.NLog.ServiceBus.Monitor.Mapping.Strategies;
using Evvosoft.Logging.Azure.Classic.NLog.Tests.Mapping.Strategies.Theories;
using Evvosoft.Logging.Azure.Classic.Shared.Model.Monitor;
using NLog;
using NLog.Common;
using Xunit;

namespace Evvosoft.Logging.Azure.Classic.NLog.Tests.Mapping.Strategies
{
    public class MonitorDtoMapStrategyTest : StrategyTestBase
    {
        private readonly IMessageMappingStrategy _strategy;

        public MonitorDtoMapStrategyTest()
        {
            _strategy = new MonitorDtoMapStrategy();
        }

        [Theory]
        [ClassData(typeof(LogEventEmptyParamsClassData))]
        public void EmptyOrInvalid_Params_WontPassCheck(object[] objects)
        {
            // arrange
            var logEvent =
                new AsyncLogEventInfo(
                    new LogEventInfo(LogLevel.Debug, "logger", null, "message", objects), null);

            // act
            var canMap = _strategy.CanMap(logEvent);

            // assert
            Assert.False(canMap);
        }

        [Fact]
        public void Valid_Params_WillPassCheck()
        {
            // arrange
            var dto = new MonitorMessageDto();
            var logEvent =
                new AsyncLogEventInfo(
                    new LogEventInfo(LogLevel.Debug, "logger", null, "message", new object[] {dto}), null);

            // act
            var canMap = _strategy.CanMap(logEvent);

            // assert
            Assert.True(canMap);
        }

        [Fact]
        public void Valid_Message_WillBeMapped()
        {
            // arrange
            var msgDto = new MonitorMessageDto {Message = "test"};
            var logEvent =
                new AsyncLogEventInfo(
                    new LogEventInfo(LogLevel.Debug, "logger", null, "message", new object[] {msgDto}), null);

            // act
            var message = _strategy.Map(logEvent);

            // assert
            Assert.NotNull(message);
            var msgDtoDeserialized = GetMonitorMsg(message.Body);
            Assert.Equal(msgDto.Message, msgDtoDeserialized.Message);
        }
    }
}
