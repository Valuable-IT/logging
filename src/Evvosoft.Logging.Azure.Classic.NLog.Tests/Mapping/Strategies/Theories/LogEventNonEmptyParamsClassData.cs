using System;
using System.Collections;
using System.Collections.Generic;

namespace Evvosoft.Logging.Azure.Classic.NLog.Tests.Mapping.Strategies.Theories
{
    public class LogEventNonEmptyParamsClassData : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[] {new object[] { }};
            yield return new object[] {new object[] {null}};
            yield return new object[] {new object[] {new Exception()}};
            yield return new object[] {new object[] {new Exception(), "test"}};
            yield return new object[] {new object[] {"test1", "test2"}};
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
