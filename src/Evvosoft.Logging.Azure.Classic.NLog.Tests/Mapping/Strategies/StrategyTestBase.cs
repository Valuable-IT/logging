using System.Text;
using Evvosoft.Logging.Azure.Classic.Shared.Model.Monitor;
using Newtonsoft.Json;

namespace Evvosoft.Logging.Azure.Classic.NLog.Tests.Mapping.Strategies
{
    public abstract class StrategyTestBase
    {
        protected MonitorMessageDto GetMonitorMsg(byte[] sbPayload)
        {
            var bodyStr = Encoding.UTF8.GetString(sbPayload);
            var dto = JsonConvert.DeserializeObject<MonitorMessageDto>(bodyStr);
            return dto;
        }
    }
}
