using System;
using Evvosoft.Logging.Azure.Classic.NLog.ServiceBus.Interfaces.Mapping;
using Evvosoft.Logging.Azure.Classic.NLog.ServiceBus.Monitor.Mapping.Strategies;
using Evvosoft.Logging.Azure.Classic.NLog.ServiceBus.Targets.Mapping;
using Evvosoft.Logging.Azure.Classic.Shared.Model.Monitor;
using NLog;
using NLog.Common;
using Xunit;

namespace Evvosoft.Logging.Azure.Classic.NLog.Tests.Mapping
{
    public class LogEventToMessageMapperTest
    {
        private const int MessageTimeToLiveInHours = 10;
        private readonly ILogEventToMessageMapper _mapper;

        public LogEventToMessageMapperTest()
        {
            var msgTimeToLive = TimeSpan.FromHours(MessageTimeToLiveInHours);
            _mapper = new LogEventToMessageMapper(new[] {new MonitorDtoMapStrategy()}, msgTimeToLive);
        }

        [Fact]
        public void MonitorMessageDto_Should_Be_Mapped()
        {
            // arrange
            var msgDto = new MonitorMessageDto();
            var logEvent =
                new AsyncLogEventInfo(
                    new LogEventInfo(LogLevel.Debug, "logger", null, "message", new object[] {msgDto}), null);

            // act
            var sbMessage = _mapper.Map(logEvent);

            // assert
            Assert.NotNull(sbMessage);
            Assert.Equal(MessageTimeToLiveInHours, sbMessage.TimeToLive.Hours);
        }
    }
}
