using System;
using Evvosoft.Logging.Azure.Classic.NLog.ServiceBus.Monitor.Mapping;
using Evvosoft.Logging.Azure.Classic.NLog.Tests.Mapping.Theories;
using Evvosoft.Logging.Azure.Classic.Shared.Model.Mapping;
using Evvosoft.Logging.Azure.Classic.Shared.Model.Monitor.Priority;
using NLog;
using Xunit;

namespace Evvosoft.Logging.Azure.Classic.NLog.Tests.Mapping
{
    public class LogLevelMappingTest
    {
        [Theory]
        [ClassData(typeof(LogLevelToPriorityClassData))]
        public void LogLevel_To_Priority_IsMapped(LogLevel level, MonitorPriorityType priority)
        {
            Assert.Equal(priority, LogLevelMapping.MapLevel(level));
        }

        [Theory]
        [ClassData(typeof(PriorityToLogLevelClassData))]
        public void Priority_To_LogLevel_IsMapped(MonitorPriorityType priority, LogLevel level)
        {
            Assert.Equal(level, LogLevelMapping.MapLevel(priority));
        }

        [Fact]
        public void OutOfRangeMessagePriority_Throws()
        {
            // act
            var action = new Action(() => LogLevelMapping.MapLevel((MonitorPriorityType)100));

            // assert
            Assert.Throws<ArgumentOutOfRangeException>(action);
        }
    }
}
