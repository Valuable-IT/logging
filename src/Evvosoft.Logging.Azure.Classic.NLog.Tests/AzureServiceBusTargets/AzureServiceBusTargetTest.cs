using Evvosoft.Logging.Azure.Classic.NLog.ServiceBus.Interfaces.Mapping;
using Evvosoft.Logging.Azure.Classic.NLog.ServiceBus.Targets;
using Evvosoft.Logging.Azure.Classic.NLog.ServiceBus.Targets.Exceptions;
using Evvosoft.Logging.Azure.Classic.NLog.ServiceBus.Targets.MessagesPortion.Interfaces;
using Evvosoft.Logging.Azure.Classic.NLog.Tests.AzureServiceBusTargets.Stub;
using Evvosoft.Logging.Azure.Classic.NLog.Tests.Targets;
using Microsoft.Azure.ServiceBus;
using Microsoft.Azure.ServiceBus.Core;
using Moq;
using NLog;
using NLog.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Evvosoft.Logging.Azure.Classic.NLog.Tests.AzureServiceBusTargets
{
    public class AzureServiceBusTargetTest : BaseTargetsTest
    {
        private readonly Mock<IMessageSender> _msgSendMock;
        private readonly Mock<ILogEventToMessageMapper> _logMapMock;
        private readonly Mock<IMessagesPortionFormer> _msgPortionMock;

        private readonly Type _targetType = typeof(AzureServiceBusTarget);
        private readonly AzureServiceBusTarget _target;
        private readonly AzureServiceBusTarget _sbNullTarget;

        public AzureServiceBusTargetTest()
        {
            _msgSendMock = new Mock<IMessageSender>();
            _logMapMock = new Mock<ILogEventToMessageMapper>();
            _msgPortionMock = new Mock<IMessagesPortionFormer>();

            _target = new AzureServiceBusTarget(
                _msgSendMock.Object,
                _logMapMock.Object,
                _msgPortionMock.Object);

            _sbNullTarget = new AzureServiceBusTarget(
                null,
                _logMapMock.Object,
                _msgPortionMock.Object);
        }

        [Fact]
        public void Initialize_Throws_OnNull_ConnectionString()
        {
            Action act = () => { InvokeInitializeTarget(_targetType, _sbNullTarget); };
            
            Assert.Throws<ArgumentNullException>(act);
        }

        [Fact]
        public void Initialize_Throws_OnNull_Queue()
        {
            _sbNullTarget.ConnectionString = "test";

            Action act = () => { InvokeInitializeTarget(_targetType, _sbNullTarget); };

            Assert.Throws<ArgumentNullException>(act);
        }

        [Fact]
        public void Initialize_Throws_OnZero_ClientTimeoutInSeconds()
        {
            _sbNullTarget.ConnectionString = "test";
            _sbNullTarget.Queue = "test";
            _sbNullTarget.ClientTimeoutInSeconds = 0;

            Action act = () => { InvokeInitializeTarget(_targetType, _sbNullTarget); };

            Assert.Throws<ArgumentOutOfRangeException>(act);
        }

        [Fact]
        public void WriteSmallSet_Works_AsExpected()
        {
            // arrange
            var logEvent = new LogEventInfo(LogLevel.Debug, "loggerName", "message");
            var asyncLogEvent = new List<AsyncLogEventInfo>
            {
                new AsyncLogEventInfo(logEvent, null),
                new AsyncLogEventInfo(logEvent, null)
            };
            var mappedMessages = new List<Message>
            {
                new Message(),
                new Message()
            };

            _target.MessageMaximumSizePerBatch = 10;
            _msgPortionMock.Setup(x => x.TakePortion(
                    It.IsAny<IEnumerable<Message>>(),
                    It.IsAny<long>(), It.Is<int>(i => i == 0)))
                .Returns(mappedMessages);

            // act
            InvokeWriteSet(_targetType, _target, asyncLogEvent);

            // assert
            _msgSendMock.Verify(x => x.SendAsync(It.Is<IList<Message>>(
                m => m.SequenceEqual(mappedMessages))), Times.Once);
        }

        [Fact]
        public void WriteTooBigSet_Throws()
        {
            // arrange
            var logEvent = new LogEventInfo(LogLevel.Debug, "loggerName", "message");

            Exception internalException = null;
            var asyncLogEvent = new List<AsyncLogEventInfo>
            {
                new AsyncLogEventInfo(logEvent, ex => { internalException = ex;}),
                new AsyncLogEventInfo(logEvent, null)
            };
            var mappedMessages = new List<Message>
            {
                new Message(new byte[]{1,2,3}),
                new Message(new byte[]{1,2,3})
            };

            _target.MessageMaximumSizePerBatch = 2;
            _msgPortionMock.Setup(x => x.TakePortion(
                    It.IsAny<IEnumerable<Message>>(),
                    It.IsAny<long>(), It.Is<int>(i => i == 0)))
                .Returns(mappedMessages);

            // act
            InvokeWriteSet(_targetType, _target, asyncLogEvent);

            // assert
            Assert.NotNull(internalException);
            Assert.IsType<MessageIsTooBigException>(internalException);
        }

        [Fact]
        public void Write_Works_AsExpected()
        {
            // arrange
            var logEvent = new LogEventInfo(LogLevel.Debug, "loggerName", "message");

            var asyncLogEvent = new AsyncLogEventInfo(logEvent, null);
            var mappedMessage = new Message(new byte[] {1, 2, 3});

            _target.MessageMaximumSizePerBatch = 10;
            _logMapMock.Setup(x => x.Map(It.IsAny<AsyncLogEventInfo>())).Returns(mappedMessage);

            // act
            InvokeWrite(_targetType, _target, asyncLogEvent);

            // assert
            _msgSendMock.Verify(x => x.SendAsync(
                It.Is<Message>(m => m == mappedMessage)), Times.Once);
        }

        [Fact]
        public void WriteTooBig_Throws()
        {
            // arrange
            var logEvent = new LogEventInfo(LogLevel.Debug, "loggerName", "message");

            Exception internalException = null;
            var asyncLogEvent = new AsyncLogEventInfo(logEvent, ex => { internalException = ex; });
            var mappedMessage = new Message(new byte[] {1, 2, 3});

            _target.MessageMaximumSizePerBatch = 2;
            _logMapMock.Setup(x => x.Map(It.IsAny<AsyncLogEventInfo>())).Returns(mappedMessage);

            // act
            InvokeWrite(_targetType, _target, asyncLogEvent);

            // assert
            Assert.NotNull(internalException);
            Assert.IsType<MessageIsTooBigException>(internalException);
        }

        [Fact]
        public void FlushAsync_Works_AsExpected()
        {
            // act
            InvokeFlushAsync(_targetType, _target, e => { });

            // assert
            _msgSendMock.Verify(x => x.CloseAsync(), Times.Once);
        }

        [Fact]
        public void FlushAsync_None_Calls_When_SB_Closed()
        {
            // arrange
            var stubTarget = new AzureServiceBusTarget(new MsgSenderStub(), _logMapMock.Object, _msgPortionMock.Object);

            // act
            InvokeFlushAsync(_targetType, stubTarget, e => { });

            // assert
            _msgSendMock.Verify(x => x.CloseAsync(), Times.Never);
        }
    }
}
