using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Azure.ServiceBus;
using Microsoft.Azure.ServiceBus.Core;

namespace Evvosoft.Logging.Azure.Classic.NLog.Tests.AzureServiceBusTargets.Stub
{
    public class MsgSenderStub : IMessageSender
    {
        public Task CloseAsync()
        {
            throw new NotImplementedException();
        }

        public void RegisterPlugin(ServiceBusPlugin serviceBusPlugin)
        {
            throw new NotImplementedException();
        }

        public void UnregisterPlugin(string serviceBusPluginName)
        {
            throw new NotImplementedException();
        }

        public string ClientId { get; }
        public bool IsClosedOrClosing => true;
        public string Path { get; }
        public TimeSpan OperationTimeout { get; set; }
        public ServiceBusConnection ServiceBusConnection { get; }
        public IList<ServiceBusPlugin> RegisteredPlugins { get; }

        public Task SendAsync(Message message)
        {
            throw new NotImplementedException();
        }

        public Task SendAsync(IList<Message> messageList)
        {
            throw new NotImplementedException();
        }

        public Task<long> ScheduleMessageAsync(Message message, DateTimeOffset scheduleEnqueueTimeUtc)
        {
            throw new NotImplementedException();
        }

        public Task CancelScheduledMessageAsync(long sequenceNumber)
        {
            throw new NotImplementedException();
        }
    }
}
