using System.Collections.Generic;
using System.Linq;
using Evvosoft.Logging.Azure.Classic.NLog.ServiceBus.Targets.MessagesPortion;
using Xunit;

namespace Evvosoft.Logging.Azure.Classic.NLog.Tests.MessagesPortion
{
    public class SplitIntoChunksExtensionTest
    {
        [Fact]
        public void Split_Works_AsExpected()
        {
            const int chunkSize = 2;

            // arrange
            var list = new List<int> { 1, 2, 3, 5, 6 };

            // act
            var chunks = list.SplitIntoChunks(chunkSize).ToList();

            // assert
            Assert.Equal(new[] { list.GetRange(0, 2), list.GetRange(2, 2), list.GetRange(4, 1) }, chunks);
        }
    }
}
