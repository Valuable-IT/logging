using Evvosoft.Logging.Classic.Database.Targets;
using Evvosoft.Logging.Classic.Database.Targets.Configuration;
using NLog;
using NLog.Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Xunit;

namespace Evvosoft.Logging.Azure.Classic.NLog.Tests.Targets
{
    public class DatabaseTargetTests : BaseTargetsTest
    {
        private static readonly Type TargetType = typeof(DatabaseTarget);
        private readonly IDatabaseTargetConfiguration _target;

        public DatabaseTargetTests()
        {
            _target = new DatabaseTarget();
        }

        [Fact]
        public void DatabaseTarget_Configuration_CstrIsNull()
        {
            _target.KeepAlive = true;
            _target.SqlTableName = "SqlTableName";
            _target.SqlAttTableName = "SqlAttTableName";

            var action = new Action(() => _target.CheckTargetConfiguration());

            Assert.Throws<ArgumentNullException>(action);
        }

        [Fact]
        public void DatabaseTarget_Configuration_MonitorTable_IsNull()
        {
            _target.KeepAlive = true;
            _target.SqlAttTableName = "SqlAttTableName";
            _target.ConnectionString = "Data Source=DbSrv;Initial Catalog=Logs;Persist Security Info=True;";

            var action = new Action(() => _target.CheckTargetConfiguration());

            Assert.Throws<ArgumentNullException>(action);
        }

        [Fact]
        public void DatabaseTarget_Configuration_MonitorTableAttr_IsNull()
        {
            _target.KeepAlive = true;
            _target.SqlTableName = "SqlTableName";
            _target.ConnectionString = "Data Source=DbSrv;Initial Catalog=Logs;Persist Security Info=True;";

            var action = new Action(() => _target.CheckTargetConfiguration());

            Assert.Throws<ArgumentNullException>(action);
        }

        [Fact]
        public void Write_SqlException_Throws()
        {
            // arrange
            var logEvent = new LogEventInfo(LogLevel.Debug, "loggerName", "message");

            Exception internalException = null;
            var asyncLogEvent = new AsyncLogEventInfo(logEvent, ex => { internalException = ex; });

            _target.KeepAlive = true;
            _target.SqlTableName = "SqlTableName";
            _target.SqlAttTableName = "SqlAttTableName";
            _target.ConnectionString = "Data Source=DbSrv;Initial Catalog=Logs;Persist Security Info=True;Connection Timeout=1;";

            InvokeInitializeTarget(TargetType, _target);

            // act
            InvokeWrite(TargetType, _target, asyncLogEvent);

            // assert
            Assert.NotNull(internalException);
            Assert.IsType<SqlException>(internalException);
        }

        [Fact]
        public void WriteSet_SqlException_Throws()
        {
            // arrange
            var logEvent = new LogEventInfo(LogLevel.Debug, "loggerName", "message");

            Exception internalException = null;
            var asyncLogEvent = new List<AsyncLogEventInfo>() { new AsyncLogEventInfo(logEvent, ex => { internalException = ex; }) };

            _target.KeepAlive = true;
            _target.SqlTableName = "SqlTableName";
            _target.SqlAttTableName = "SqlAttTableName";
            _target.ConnectionString = "Data Source=DbSrv;Initial Catalog=Logs;Persist Security Info=True;Connection Timeout=1;";

            InvokeInitializeTarget(TargetType, _target);

            // act
            InvokeWriteSet(TargetType, _target, asyncLogEvent);

            // assert
            Assert.NotNull(internalException);
            Assert.IsType<SqlException>(internalException);
        }
    }
}
