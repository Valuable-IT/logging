using NLog.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Evvosoft.Logging.Azure.Classic.NLog.Tests.Targets
{
    public abstract class BaseTargetsTest
    {
        private const BindingFlags BindingFlags = System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic;

        protected void InvokeInitializeTarget(Type targetType, object target)
        {
            var method = targetType
                .GetMethods(BindingFlags)
                .First(x => x.Name == "InitializeTarget");

            try
            {
                method.Invoke(target, new object[] { });
            }
            catch (TargetInvocationException e)
            {
                if (null != e.InnerException)
                    throw e.InnerException;
                throw;
            }
        }

        protected void InvokeWriteSet(Type targetType, object target, List<AsyncLogEventInfo> asyncLogEvent)
        {
            var method = targetType
                .GetMethods(BindingFlags)
                .Where(x => x.Name == "Write")
                .First(x => x.GetParameters().Any(p => p.ParameterType.IsGenericType));

            method.Invoke(target, new object[] { asyncLogEvent });
        }

        protected void InvokeWrite(Type targetType, object target, AsyncLogEventInfo asyncLogEvent)
        {
            var method = targetType
                .GetMethods(BindingFlags)
                .Where(x => x.Name == "Write")
                .First(x => x.GetParameters().Any(p => p.ParameterType == asyncLogEvent.GetType()));

            method.Invoke(target, new object[] { asyncLogEvent });
        }

        protected void InvokeFlushAsync(Type targetType, object target, AsyncContinuation asyncContinuation)
        {
            var method = targetType
                .GetMethods(BindingFlags)
                .Where(x => x.Name == "FlushAsync")
                .First(x => x.GetParameters().Any(p => p.ParameterType == asyncContinuation.GetType()));

            method.Invoke(target, new object[] { asyncContinuation });
        }
    }
}
