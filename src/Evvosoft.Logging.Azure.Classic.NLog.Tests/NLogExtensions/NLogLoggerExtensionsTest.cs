using System;
using Evvosoft.Logging.Azure.Classic.Shared.Model.Monitor;
using Evvosoft.Logging.Azure.Classic.Shared.Model.Monitor.Priority;
using Evvosoft.Logging.Azure.Classic.Shared.Model.NLogExtensions;
using Moq;
using NLog;
using Xunit;

namespace Evvosoft.Logging.Azure.Classic.NLog.Tests.NLogExtensions
{
    public class NLogLoggerExtensionsTest
    {
        private readonly Mock<ILogger> _logMock;

        public NLogLoggerExtensionsTest()
        {
            _logMock = new Mock<ILogger>();
        }

        [Fact]
        public void NullMessage_WontBe_Mapped()
        {
            // act
            NLogLoggerExtensions.Log(_logMock.Object, null);

            // assert
            _logMock.Verify(x => x.Log(It.IsAny<LogLevel>(), It.IsAny<object>()), Times.Never);
        }

        [Fact]
        public void MessagePriority_Mapped_AsExpected()
        {
            // arrange
            var dto = new MonitorMessageDto {Priority = MonitorPriorityType.Major, Date = DateTime.Now};

            // act
            _logMock.Object.Log(dto);

            // assert
            _logMock.Verify(x => x.Log(
                It.IsAny<Type>(),
                It.Is<LogEventInfo>(
                    e => e.Level == LogLevel.Error &&
                         e.TimeStamp == dto.Date &&
                         e.Parameters[0] == dto)), Times.Once);
        }
    }
}
