using Evvosoft.Logging.Azure.Shared.Model.Mapping;
using Evvosoft.Logging.Azure.Shared.Model.Logging;
using Evvosoft.Logging.Database.Targets.Configuration;
using Evvosoft.Logging.DataLayer;
using NLog.Common;
using NLog.Config;
using NLog.Targets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Evvosoft.Logging.Database.Targets
{
    [Target("DatabaseTgt")]
    public class DatabaseTarget : TargetWithLayout, IDatabaseTargetConfiguration
    {
        private readonly string _internalLogPrefix = $"{typeof(DatabaseTarget).Name}:";

        private MonitorEventStorage _monitorEventStorage;

        [RequiredParameter]
        public string ConnectionString { get; set; }

        [RequiredParameter]
        public string SqlAttTableName { get; set; }

        [RequiredParameter]
        public string SqlTableName { get; set; }

        public bool KeepAlive { get; set; } = false;

        public void CheckTargetConfiguration()
        {
            if (string.IsNullOrEmpty(ConnectionString))
            {
                throw new ArgumentNullException(nameof(ConnectionString));
            }

            if (string.IsNullOrEmpty(SqlAttTableName))
            {
                throw new ArgumentNullException(nameof(SqlAttTableName));
            }

            if (string.IsNullOrEmpty(SqlTableName))
            {
                throw new ArgumentNullException(nameof(SqlTableName));
            }
        }

        protected override void InitializeTarget()
        {
            base.InitializeTarget();

            CheckTargetConfiguration();

            _monitorEventStorage = new MonitorEventStorage(ConnectionString, SqlTableName, SqlAttTableName, KeepAlive);
        }

        protected override void Write(AsyncLogEventInfo logEvent)
        {
            try
            {
                _monitorEventStorage
                    .InsertMessages(new List<MonitorMessageDto> { MonitorMessageMapping.Map(logEvent) }, CancellationToken.None)
                    .ConfigureAwait(false)
                    .GetAwaiter()
                    .GetResult();
            }
            catch (Exception ex)
            {
                InternalLogger.Error(
                    $"{_internalLogPrefix} An error occured while sending message to Service Bus target, {ex}");

                logEvent.Continuation(ex);
            }
        }

        protected override void Write(IList<AsyncLogEventInfo> logEvents)
        {
            try
            {
                var msgSet = logEvents.Select(MonitorMessageMapping.Map).ToList();

                _monitorEventStorage
                    .InsertMessages(msgSet, CancellationToken.None)
                    .ConfigureAwait(false)
                    .GetAwaiter()
                    .GetResult();
            }
            catch (Exception ex)
            {
                InternalLogger.Error(
                    $"{_internalLogPrefix} An error occured while sending message to Service Bus target, {ex}");

                logEvents.First().Continuation(ex);
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (disposing)
            {
                _monitorEventStorage.Dispose();
            }
        }
    }
}
