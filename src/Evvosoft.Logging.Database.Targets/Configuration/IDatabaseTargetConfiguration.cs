namespace Evvosoft.Logging.Database.Targets.Configuration
{
    /// <summary>
    /// Represents list of configurations to work database logging target
    /// </summary>
    public interface IDatabaseTargetConfiguration
    {
        /// <summary>
        /// Connection string to target database
        /// </summary>
        string ConnectionString { get; set; }

        /// <summary>
        /// Indicates whether to keep the database connection open between the log events.
        /// </summary>
        bool KeepAlive { get; set; }

        /// <summary>
        /// Monitor attributes table name, additional table which is used to store optional event attributes
        /// </summary>
        string SqlAttTableName { get; set; }

        /// <summary>
        /// Monitor table name, where logged monitor events will be written
        /// </summary>
        string SqlTableName { get; set; }

        /// <summary>
        /// Implements mehod to cheking state of target configuration
        /// </summary>
        void CheckTargetConfiguration();
    }
}
