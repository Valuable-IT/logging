using System;
using System.Collections.Generic;
using System.Linq;
using Evvosoft.Logging.Azure.NLog.ServiceBus.Targets.MessagesPortion;
using Evvosoft.Logging.Azure.NLog.ServiceBus.Targets.MessagesPortion.Interfaces;
using Microsoft.Azure.ServiceBus;
using Xunit;

namespace Evvosoft.Logging.Azure.NLog.Tests.MessagesPortion
{
    public class MessagesPortionFormerTest
    {
        private readonly IMessagesPortionFormer _portionFormer;

        public MessagesPortionFormerTest()
        {
            _portionFormer = new MessagesPortionFormer();
        }

        [Fact]
        public void MessagePortion_Throws_WhenInput_Is_Null()
        {
            // act
            Action act = () => _portionFormer.TakePortion(null, 0);

            // assert
            Assert.Throws<ArgumentNullException>(act);
        }

        [Fact]
        public void ReturnsNone_For_OversizedMessages()
        {
            const int bytesPerMsg = 200;

            // arrange
            var messages = GenerateMessages(bytesPerMsg, 5);

            // act
            var portion = _portionFormer.TakePortion(messages, 1).ToList();

            // assert
            Assert.NotNull(portion);
            Assert.Empty(portion);
        }


        [Fact]
        public void ExactMessagesCount_InPortion_AreProvided()
        {
            const int bytesPerMsg = 200;
            const int messagesInPortion = 2;
            const int portionSize = messagesInPortion * bytesPerMsg;

            // arrange
            var messages = GenerateMessages(bytesPerMsg, 5);

            // act
            var portion = _portionFormer.TakePortion(messages, portionSize);

            // assert
            Assert.Equal(messages.Take(messagesInPortion), portion);
        }

        [Fact]
        public void PortionSkip_Works_AsExpected()
        {
            const int bytesPerMsg = 200;
            const int messagesInPortion = 2;
            const int messagesToSkip = 2;
            const int portionSize = messagesInPortion * bytesPerMsg;

            // arrange
            var messages = GenerateMessages(bytesPerMsg, 5).ToList();

            // act
            var portion = _portionFormer.TakePortion(messages, portionSize, messagesToSkip);

            // assert
            Assert.Equal(messages.GetRange(messagesToSkip, messagesInPortion), portion);
        }

        [Fact]
        public void AllMessages_WillBe_Split_PerPortion()
        {
            const int bytesPerMsg = 200;
            const int messagesInPortion = 2;
            const int portionSize = messagesInPortion * bytesPerMsg + 10;

            // arrange
            var messages = GenerateMessages(bytesPerMsg, 5).ToList();

            // act
            var portions = new List<List<Message>>();
            var skipN = 0;

            while (true)
            {
                var portion = _portionFormer.TakePortion(messages, portionSize, skipN).ToList();
                if (!portion.Any())
                    break;

                portions.Add(portion);
                skipN += portion.Count;
            }

            // assert
            Assert.Equal(new[] { messages.GetRange(0, 2), messages.GetRange(2, 2), messages.GetRange(4, 1) }, portions);
        }

        /// <summary>
        /// Forms test message set
        /// </summary>
        /// <param name="msgSize">Message size</param>
        /// <param name="count">Messages count to form</param>
        /// <returns>Message set</returns>
        private static IList<Message> GenerateMessages(int msgSize, int count)
        {
            var bytes = Enumerable.Range(1, msgSize)
                .Select(i => (byte)i)
                .ToArray();

            return Enumerable.Range(0, count)
                .Select(n => new Message(bytes) { MessageId = n.ToString() })
                .ToList();
        }
    }
}
