using System.Text;
using Evvosoft.Logging.Azure.Shared.Model.Logging;
using Newtonsoft.Json;

namespace Evvosoft.Logging.Azure.NLog.Tests.Mapping.Strategies
{
    public abstract class StrategyTestBase
    {
        protected MonitorMessageDto GetMonitorMsg(byte[] sbPayload)
        {
            var bodyStr = Encoding.UTF8.GetString(sbPayload);
            var dto = JsonConvert.DeserializeObject<MonitorMessageDto>(bodyStr);
            return dto;
        }
    }
}
