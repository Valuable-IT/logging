using Evvosoft.Logging.Azure.NLog.ServiceBus.Interfaces.Mapping.Strategies;
using Evvosoft.Logging.Azure.NLog.ServiceBus.Monitor.Mapping.Strategies;
using Evvosoft.Logging.Azure.NLog.Tests.Mapping.Strategies.Theories;
using Evvosoft.Logging.Azure.Shared.Model.Mapping;
using Evvosoft.Logging.Azure.Shared.Model.Logging;
using NLog;
using NLog.Common;
using System;
using Xunit;

namespace Evvosoft.Logging.Azure.NLog.Tests.Mapping.Strategies
{
    public class LogWithParamsMapStrategyTest : StrategyTestBase
    {
        private readonly IMessageMappingStrategy _strategy;

        public LogWithParamsMapStrategyTest()
        {
            _strategy = new LogWithParamsMapStrategy();
        }

        [Theory]
        [ClassData(typeof(LogEventNonEmptyParamsClassData))]
        public void Defined_Params_PassCheck(object[] objects)
        {
            // arrange
            var logEvent =
                new AsyncLogEventInfo(
                    new LogEventInfo(LogLevel.Debug, "logger", null, "message", objects), null);

            // act
            var canMap = _strategy.CanMap(logEvent);

            // assert
            Assert.True(canMap);
        }

        [Fact]
        public void Defined_MonitorParam_WontPassCheck()
        {
            // arrange
            var paramObj = new object[] {new MonitorMessageDto()};

            var logEvent =
                new AsyncLogEventInfo(
                    new LogEventInfo(LogLevel.Debug, "logger", null, "message", paramObj), null);

            // act
            var canMap = _strategy.CanMap(logEvent);

            // assert
            Assert.False(canMap);
        }

        [Fact]
        public void LogMessage_Without_Params_Is_Mapped_AsExpected()
        {
            const string message = "message";
            var level = LogLevel.Info;
            var dtNow = DateTime.UtcNow;

            // arrange

            var logEvent = new LogEventInfo(level, null, null, message, null, null) {TimeStamp = dtNow};
            var logInfo = new AsyncLogEventInfo(logEvent, null);

            // act
            var sbMessage = _strategy.Map(logInfo);

            // assert
            var msgDto = GetMonitorMsg(sbMessage.Body);
            Assert.Equal(dtNow, msgDto.Date);
            Assert.Equal(message, msgDto.Message);
            Assert.Equal(LogLevelMapping.MapLevel(level), msgDto.Priority);
        }

        [Fact]
        public void LogMessage_WithException_Is_Mapped_AsExpected()
        {
            const string message = "message";
            const string excMessage = "exception message";
            var level = LogLevel.Info;
            var dtNow = DateTime.UtcNow;

            // arrange

            var logEvent = new LogEventInfo(level, null, null, message, null, new Exception(excMessage)) { TimeStamp = dtNow };
            var logInfo = new AsyncLogEventInfo(logEvent, null);

            // act
            var sbMessage = _strategy.Map(logInfo);

            // assert
            var msgDto = GetMonitorMsg(sbMessage.Body);
            Assert.Equal(dtNow, msgDto.Date);
            Assert.Equal(message, msgDto.Message);
            Assert.Contains(excMessage, msgDto.Description);
            Assert.Equal(LogLevelMapping.MapLevel(level), msgDto.Priority);
        }

        [Fact]
        public void LogMessage_With_Params_Is_Mapped_AsExpected()
        {
            const string message = "message {0}";
            var paramObj = new object[] {"test"};
            const string expectedMessage = "message test";
            var level = LogLevel.Info;
            var dtNow = DateTime.UtcNow;

            // arrange

            var logEvent = new LogEventInfo(level, null, null, message, paramObj, null) { TimeStamp = dtNow };
            var logInfo = new AsyncLogEventInfo(logEvent, null);

            // act
            var sbMessage = _strategy.Map(logInfo);

            // assert
            var msgDto = GetMonitorMsg(sbMessage.Body);
            Assert.Equal(dtNow, msgDto.Date);
            Assert.Equal(expectedMessage, msgDto.Message);
            Assert.Equal(LogLevelMapping.MapLevel(level), msgDto.Priority);
        }
    }
}
