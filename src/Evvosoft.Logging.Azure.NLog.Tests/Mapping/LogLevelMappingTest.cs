using Evvosoft.Logging.Azure.NLog.Tests.Mapping.Theories;
using Evvosoft.Logging.Azure.Shared.Model.Mapping;
using Evvosoft.Logging.Azure.Shared.Model.Logging;
using NLog;
using System;
using Xunit;

namespace Evvosoft.Logging.Azure.NLog.Tests.Mapping
{
    public class LogLevelMappingTest
    {
        [Theory]
        [ClassData(typeof(LogLevelToPriorityClassData))]
        public void LogLevel_To_Priority_IsMapped(LogLevel level, MonitorPriority priority)
        {
            Assert.Equal(priority, LogLevelMapping.MapLevel(level));
        }

        [Theory]
        [ClassData(typeof(PriorityToLogLevelClassData))]
        public void Priority_To_LogLevel_IsMapped(MonitorPriority priority, LogLevel level)
        {
            Assert.Equal(level, LogLevelMapping.MapLevel(priority));
        }

        [Fact]
        public void OutOfRangeMessagePriority_Throws()
        {
            // act
            var action = new Action(() => LogLevelMapping.MapLevel((MonitorPriority)100));

            // assert
            Assert.Throws<ArgumentOutOfRangeException>(action);
        }
    }
}
