using System.Collections;
using System.Collections.Generic;
using Evvosoft.Logging.Azure.Shared.Model.Logging;
using NLog;

namespace Evvosoft.Logging.Azure.NLog.Tests.Mapping.Theories
{
    public class PriorityToLogLevelClassData : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[] {MonitorPriority.Undefined, LogLevel.Off};
            yield return new object[] {MonitorPriority.Blocker, LogLevel.Fatal};
            yield return new object[] {MonitorPriority.Major, LogLevel.Error};
            yield return new object[] {MonitorPriority.Medium, LogLevel.Warn};
            yield return new object[] {MonitorPriority.Low, LogLevel.Info};
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }

    public class LogLevelToPriorityClassData : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[] {LogLevel.Off, MonitorPriority.Undefined};
            yield return new object[] {LogLevel.Fatal, MonitorPriority.Blocker};
            yield return new object[] {LogLevel.Error, MonitorPriority.Major};
            yield return new object[] {LogLevel.Warn, MonitorPriority.Medium};
            yield return new object[] {LogLevel.Info, MonitorPriority.Low};
            yield return new object[] {LogLevel.Debug, MonitorPriority.Low};
            yield return new object[] {LogLevel.Trace, MonitorPriority.Low};
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
