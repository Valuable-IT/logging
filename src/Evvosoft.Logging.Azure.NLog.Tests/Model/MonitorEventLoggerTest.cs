using Evvosoft.Logging.Azure.Shared.Model.Logging;
using Moq;
using NLog;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Evvosoft.Logging.Azure.NLog.Tests.Model
{
    public class MonitorEventLoggerTest
    {
        private readonly IEventLogger _eventLogger;
        private readonly Mock<ILogger> _log;

        public MonitorEventLoggerTest()
        {
            _log = new Mock<ILogger>();
            _eventLogger = new MonitorEventLogger(_log.Object);
        }

        [Fact]
        public void Should_Log_Empty_Message_Test()
        {
            _eventLogger.Log(new MonitorMessageDto() , false);

            _log.Verify(x => x.Log(It.IsAny<Type>(), It.IsAny<LogEventInfo>()), Times.Once);
        }

        [Fact]
        public void Should_Log_Normal_Message_Test()
        {
            _eventLogger.Log(new MonitorMessageDto() { Message="test",StackTrace="test",Description= "test" }, false);

            _log.Verify(x => x.Log(It.IsAny<Type>(), It.IsAny<LogEventInfo>()), Times.Once);
        }

        [Fact]
        public void Should_Log_Long_Message_Test()
        {
            string myString = new string('*', 100000);

            _eventLogger.Log(new MonitorMessageDto() { Message = myString, StackTrace = myString, Description = myString }, false);

            _log.Verify(x => x.Log(It.IsAny<Type>(), It.IsAny<LogEventInfo>()), Times.AtLeast(2));
        }



    }
}
