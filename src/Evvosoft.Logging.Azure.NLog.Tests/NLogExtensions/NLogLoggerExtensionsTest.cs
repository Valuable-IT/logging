using System;
using Evvosoft.Logging.Azure.Shared.Model.Logging;
using Evvosoft.Logging.Azure.Shared.Model.NLogExtensions;
using Moq;
using NLog;
using Xunit;

namespace Evvosoft.Logging.Azure.NLog.Tests.NLogExtensions
{
    public class NLogLoggerExtensionsTest
    {
        private readonly Mock<ILogger> _logMock;

        public NLogLoggerExtensionsTest()
        {
            _logMock = new Mock<ILogger>();
        }

        [Fact]
        public void NullMessage_WontBe_Mapped()
        {
            // act
            NLogLoggerExtensions.Log(_logMock.Object, null,false);

            // assert
            _logMock.Verify(x => x.Log(It.IsAny<LogLevel>(), It.IsAny<object>()), Times.Never);
        }
        
    }
}
