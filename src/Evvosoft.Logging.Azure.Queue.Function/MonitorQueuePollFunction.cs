using Evvosoft.Logging.Azure.Queue.Function.Logic.Model.Mapper;
using Evvosoft.Logging.Azure.Queue.Function.Logic.Service;
using Evvosoft.Logging.Azure.Queue.Function.Logic.Service.Data;
using Evvosoft.Logging.Azure.Queue.Function.Logic.Service.Interfaces;
using Evvosoft.Logging.DataLayer;
using Microsoft.Azure.ServiceBus.Core;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Evvosoft.Logging.Azure.Queue.Function
{
    /* The function polls Service Bus queue to fetch batch of messages.
     * Having messages fetched the function maps them to DB model and
     * insert into SQL DB using the fastest batch insertion approach.
     * The following configuration parameters needs to be specified for
     * normal operation:
     * 1. SbConnectionString -- service bus connection string (Endpoint=sb://...)
     * 2. MonitorQueueName -- the queue name which has to be polled
     * 3. SbConnectionString -- SQL server connection string (Server=tcp:srv.com)
     * 4. SqlTableName -- SQL server table name which be used as messages storage
     * 5. SqlAttTableName -- SQL server table name which be used as message's attachments storage
     */
    public static class MonitorQueuePollFunction
    {
        // Cron trigger, when and how ofter this function will be fired
        private const string TriggerCron = "*/10 * * * * *";

        // Configuration keys const
        private const string SbConnStrKey = "SbConnectionString";
        private const string QueueNameKey = "MonitorQueueName";
        private const string SqlConnStrKey = "SqlConnectionString";
        private const string SqlTableNameKey = "SqlTableName";
        private const string SqlAttTableNameKey = "SqlAttTableName";
        private const string SbClientTimeoutInSecKey = "SbClientTimeoutInSec";
        private const string MessagesInBatchKey = "MessagesInBatch";
        private const string MessagesQueueTestTimeoutInMSecKey = "MessagesQueueTestTimeoutInMSec";
        private const string MessagesBatchFormingTimeoutInSecKey = "MessagesBatchFormingTimeoutInSec";
        private const string MessagesMaxBatchesPerInvocationKey = "MessagesMaxBatchesPerInvocation";

        // Configured parameters
        private static readonly string SbConnStr;
        private static readonly string QueueName;
        private static readonly string SqlConnStr;
        private static readonly string SqlTableName;
        private static readonly string SqlAttTableName;
        private static readonly int SbClientTimeoutInSec = 60; // ServiceBus client timeout in seconds
        private static readonly int MessagesInBatch = 10; // Messages aggregated to SQL per single batch
        private static readonly int MessagesQueueTestTimeoutInMSec = 100; // How long wait until 'MessagesInBatch' will be collected
        private static readonly int MsgBatchFormTimeoutInSec = 5; // How long wait until 'MessagesInBatch' will be collected
        private static readonly int MsgBatchesMaxPerInvocation = 10; // Maximum batches which might be formed per one invocation

        private static readonly IPollQueueAndProcessService ProcessService;

        static MonitorQueuePollFunction()
        {
            SbConnStr = Environment.GetEnvironmentVariable(SbConnStrKey);
            QueueName = Environment.GetEnvironmentVariable(QueueNameKey);
            SqlConnStr = Environment.GetEnvironmentVariable(SqlConnStrKey);
            SqlTableName = Environment.GetEnvironmentVariable(SqlTableNameKey);
            SqlAttTableName = Environment.GetEnvironmentVariable(SqlAttTableNameKey);

            var sbClientTimeout = Environment.GetEnvironmentVariable(SbClientTimeoutInSecKey);
            if (!string.IsNullOrEmpty(sbClientTimeout))
            {
                int.TryParse(sbClientTimeout, out SbClientTimeoutInSec);
            }

            var msgInBatch = Environment.GetEnvironmentVariable(MessagesInBatchKey);
            if (!string.IsNullOrEmpty(msgInBatch))
            {
                int.TryParse(msgInBatch, out MessagesInBatch);
            }

            var msgBatchTimeout = Environment.GetEnvironmentVariable(MessagesBatchFormingTimeoutInSecKey);
            if (!string.IsNullOrEmpty(msgBatchTimeout))
            {
                int.TryParse(msgBatchTimeout, out MsgBatchFormTimeoutInSec);
            }

            var msgQueueTestTimeout = Environment.GetEnvironmentVariable(MessagesQueueTestTimeoutInMSecKey);
            if (!string.IsNullOrEmpty(msgQueueTestTimeout))
            {
                int.TryParse(msgQueueTestTimeout, out MessagesQueueTestTimeoutInMSec);
            }

            var msgMaxBatches = Environment.GetEnvironmentVariable(MessagesMaxBatchesPerInvocationKey);
            if (!string.IsNullOrEmpty(msgMaxBatches))
            {
                int.TryParse(msgMaxBatches, out MsgBatchesMaxPerInvocation);
            }

            // To have shared, statically defined resource to reduce function start
            ProcessService = new PollQueueAndProcessService(
                new MessageMapper(),
                MessagesInBatch,
                MessagesQueueTestTimeoutInMSec,
                MsgBatchFormTimeoutInSec);
        }

        [FunctionName("MonitorQueuePollFunction")]
        public static async Task Run(
            [TimerTrigger(TriggerCron)] TimerInfo poolTimer,
            ILogger log,
            CancellationToken ct)
        {
            log.LogInformation($"Monitor queue poll function has been triggered: {DateTime.UtcNow}");
            log.LogInformation($"Messages in batch: {MessagesInBatch}");
            log.LogInformation($"Messages batch forming timeout: {MsgBatchFormTimeoutInSec}");
            log.LogInformation($"Messages max batches per invocation: {MsgBatchesMaxPerInvocation}");

            IMessageReceiver msgRcv = null;

            try
            {
                msgRcv = new MessageReceiver(SbConnStr, QueueName)
                {
                    OperationTimeout = TimeSpan.FromSeconds(SbClientTimeoutInSec)
                };

                using (var eStorage = new MonitorEventStorage(SqlConnStr, SqlTableName, SqlAttTableName, keepAlive: true))
                {
                    var rCycleSvc = new ReceivingCycleService(msgRcv, ProcessService, eStorage, log);
                    var result = await rCycleSvc.RunCycle(MsgBatchesMaxPerInvocation, ct);

                    if (CycleServiceResultType.TooFewMessages == result)
                    {
                        log.LogInformation(
                            $"There are no or very little messages in the queue '{QueueName}' during past {MessagesQueueTestTimeoutInMSec} ms");
                        return;
                    }

                    log.LogInformation(
                        $"Max batches per invocation limit '{MsgBatchesMaxPerInvocation}' has been reached or there are no messages to process. Function has been terminated.");
                }
            }
            catch (Exception ex)
            {
                log.LogError(ex, "Function invocation was terminated due to the following error");
                throw;
            }
            finally
            {
                if (null != msgRcv)
                {
                    await msgRcv.CloseAsync();
                }
            }
        }
    }
}
