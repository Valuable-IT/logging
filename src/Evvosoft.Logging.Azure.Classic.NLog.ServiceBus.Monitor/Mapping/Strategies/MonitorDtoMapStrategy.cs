using System;
using System.Linq;
using Evvosoft.Logging.Azure.Classic.NLog.ServiceBus.Interfaces.Mapping.Strategies;
using Evvosoft.Logging.Azure.Classic.NLog.ServiceBus.Monitor.Messages;
using Evvosoft.Logging.Azure.Classic.Shared.Model.Monitor;
using Microsoft.Azure.ServiceBus;
using Newtonsoft.Json;
using NLog.Common;

namespace Evvosoft.Logging.Azure.Classic.NLog.ServiceBus.Monitor.Mapping.Strategies
{
    public class MonitorDtoMapStrategy : IMessageMappingStrategy
    {
        private static readonly Type MonitorMsgDtoType = typeof(MonitorMessageDto);

        public bool CanMap(AsyncLogEventInfo logInfo)
        {
            var parameters = logInfo.LogEvent?.Parameters;

            if (null == parameters)
                return false;

            if (!parameters.Any())
                return false;

            if (null == parameters[0])
                return false;

            // it's expected to have first parameter as full DTO message
            return parameters[0].GetType() == MonitorMsgDtoType;
        }

        public Message Map(AsyncLogEventInfo logInfo)
        {
            var msgDto = logInfo.LogEvent?.Parameters?[0];
            var jMessageBody = JsonConvert.SerializeObject(msgDto);
            return MessageBuilder.Build(jMessageBody);
        }
    }
}
