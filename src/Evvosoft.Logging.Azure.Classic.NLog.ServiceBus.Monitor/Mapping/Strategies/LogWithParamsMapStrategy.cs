using System;
using System.Linq;
using Evvosoft.Logging.Azure.Classic.NLog.ServiceBus.Interfaces.Mapping.Strategies;
using Evvosoft.Logging.Azure.Classic.NLog.ServiceBus.Monitor.Messages;
using Evvosoft.Logging.Azure.Classic.Shared.Model.Mapping;
using Evvosoft.Logging.Azure.Classic.Shared.Model.Monitor;
using Microsoft.Azure.ServiceBus;
using Newtonsoft.Json;
using NLog.Common;
using NLog.LayoutRenderers;

namespace Evvosoft.Logging.Azure.Classic.NLog.ServiceBus.Monitor.Mapping.Strategies
{
    public class LogWithParamsMapStrategy : IMessageMappingStrategy
    {
        private const string ExceptionFormatStr = @"toString";

        private const int MonitorSourceId = 1;
        private const int MonitorTypeId = 1;

        private static readonly Type MonitorMsgDtoType = typeof(MonitorMessageDto);

        public bool CanMap(AsyncLogEventInfo logInfo)
        {
            var parameters = logInfo.LogEvent?.Parameters;

            if (null == parameters)
                return true;

            if (!parameters.Any())
                return true;

            if (null == parameters[0])
                return true;

            // it's not expected to have first parameter as full DTO message
            return parameters[0].GetType() != MonitorMsgDtoType;
        }

        public Message Map(AsyncLogEventInfo logInfo)
        {
            var lEv = logInfo.LogEvent;

            string details = null;

            if (null != lEv.Exception)
            {
                var exRender = new ExceptionLayoutRenderer {Format = ExceptionFormatStr};
                details = exRender.Render(lEv);
            }

            // craft message from log event
            var msgDto = new MonitorMessageDto
            {
                Message = lEv.FormattedMessage,
                Description = details,
                StackTrace = lEv.Exception?.StackTrace,
                Date = lEv.TimeStamp,
                Priority = LogLevelMapping.MapLevel(lEv.Level),
                MonitorSourceId = MonitorSourceId,
                MonitorTypeId = MonitorTypeId
            };

            var jMessageBody = JsonConvert.SerializeObject(msgDto);
            return MessageBuilder.Build(jMessageBody);
        }
    }
}
