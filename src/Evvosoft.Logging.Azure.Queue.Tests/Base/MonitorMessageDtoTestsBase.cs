using System.IO;
using System.Reflection;

namespace Evvosoft.Logging.Azure.Queue.Tests.Base
{
    public abstract class MonitorMessageDtoTestsBase
    {
        /// <summary>
        /// Reads resource content using its full name
        /// </summary>
        /// <param name="resourceName">Resource to read</param>
        /// <param name="asm">Assembly with resource</param>
        /// <returns>Resource content</returns>
        protected static string ReadResourceContent(string resourceName, Assembly asm)
        {
            using (var stream = asm.GetManifestResourceStream(resourceName))
            {
                using (var strR = new StreamReader(stream))
                {
                    return strR.ReadToEnd();
                }
            }
        }
    }
}
