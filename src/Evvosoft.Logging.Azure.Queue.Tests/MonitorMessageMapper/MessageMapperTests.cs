using System;
using System.Text;
using Evvosoft.Logging.Azure.Queue.Function.Logic.Model.Mapper;
using Evvosoft.Logging.Azure.Queue.Function.Logic.Model.Mapper.Exceptions;
using Evvosoft.Logging.Azure.Queue.Function.Logic.Model.Mapper.Interfaces;
using Evvosoft.Logging.Azure.Queue.Tests.MonitorMessageMapper.Theories;
using Evvosoft.Logging.Azure.Shared.Model.Logging;
using Microsoft.Azure.ServiceBus;
using Newtonsoft.Json;
using Xunit;

namespace Evvosoft.Logging.Azure.Queue.Tests.MonitorMessageMapper
{
    public class MessageMapperTests
    {
        private readonly TimeZoneInfo estTimeZoneInfo;
        private readonly IMessageMapper _mapper;

        public MessageMapperTests()
        {
            estTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");

            _mapper = new MessageMapper();
        }

        [Theory]
        [ClassData(typeof(AttachmentsClassData))]
        public void CorrectMessage_IsMapped_Test(MonitorAttachmentDto[] attachments)
        {
            // arrange
            var dtNow = DateTime.UtcNow;
            var dto = new MonitorMessageDto
            {
                Message = new string('t', 3000),
                Date = dtNow,
                Priority = MonitorPriority.Low,
                MonitorType = MonitorType.General,
                MonitorSource = MonitorSource.Central,
                Attachments = attachments
            };
            var jDto = JsonConvert.SerializeObject(dto);
            var byteDto = Encoding.UTF8.GetBytes(jDto);
            var sBusMsg = new Message(byteDto);

            // act
            var mappedDto = _mapper.Map(sBusMsg);

            // assert
            Assert.NotNull(mappedDto);
            Assert.NotEqual(dto.Message, mappedDto.Message);
            Assert.Equal(dto.Message.Substring(0, 250), mappedDto.Message);
            Assert.Equal(TimeZoneInfo.ConvertTime(dto.Date, estTimeZoneInfo), mappedDto.Date);
            Assert.Equal(dto.Priority, mappedDto.Priority);
            Assert.Equal(dto.IsError, mappedDto.IsError);
        }

        [Fact]
        public void InvalidContent_MessageField_Throws_Test()
        {
            // arrange
            var dtNow = DateTime.UtcNow;
            var dto = new MonitorMessageDto
            {
                Date = dtNow,
                Priority = MonitorPriority.Low,
                MonitorType = MonitorType.General,
                MonitorSource = MonitorSource.Central,
            };
            var jDto = JsonConvert.SerializeObject(dto);
            var byteDto = Encoding.UTF8.GetBytes(jDto);
            var sBusMsg = new Message(byteDto);

            var mapAction = new Action(() => _mapper.Map(sBusMsg));

            // act
            // assert
            Assert.Throws<MessageCantBeMappedException>(mapAction);
        }


        [Fact]
        public void InvalidContent_PriorityField_Throws_Test()
        {
            // arrange
            var dtNow = DateTime.UtcNow;
            var dto = new MonitorMessageDto
            {
                Message = "test",
                Date = dtNow,
                MonitorType = MonitorType.General,
                MonitorSource = MonitorSource.Central
            };
            var jDto = JsonConvert.SerializeObject(dto);
            var byteDto = Encoding.UTF8.GetBytes(jDto);
            var sBusMsg = new Message(byteDto);

            var mapAction = new Action(() => _mapper.Map(sBusMsg));

            // act
            // assert
            Assert.Throws<MessageCantBeMappedException>(mapAction);
        }

        [Fact]
        public void InvalidContent_MonitorTypeIdField_Throws_Test()
        {
            // arrange
            var dtNow = DateTime.UtcNow;
            var dto = new MonitorMessageDto
            {
                Message = "test",
                Date = dtNow,
                Priority = MonitorPriority.Low,
                MonitorSource = MonitorSource.Central
            };
            var jDto = JsonConvert.SerializeObject(dto);
            var byteDto = Encoding.UTF8.GetBytes(jDto);
            var sBusMsg = new Message(byteDto);

            var mapAction = new Action(() => _mapper.Map(sBusMsg));

            // act
            // assert
            Assert.Throws<MessageCantBeMappedException>(mapAction);
        }

        [Fact]
        public void InvalidContent_MonitorSourceIdField_Throws_Test()
        {
            // arrange
            var dtNow = DateTime.UtcNow;
            var dto = new MonitorMessageDto
            {
                Message = "test",
                Date = dtNow,
                Priority = MonitorPriority.Low,
                MonitorType = MonitorType.Central
            };
            var jDto = JsonConvert.SerializeObject(dto);
            var byteDto = Encoding.UTF8.GetBytes(jDto);
            var sBusMsg = new Message(byteDto);

            var mapAction = new Action(() => _mapper.Map(sBusMsg));

            // act
            // assert
            Assert.Throws<MessageCantBeMappedException>(mapAction);
        }

        [Fact]
        public void InvalidAttachment_AttachmentTypeField_Throws_Test()
        {
            // arrange
            var dtNow = DateTime.UtcNow;
            var dto = new MonitorMessageDto
            {
                Message = "test",
                Date = dtNow,
                Priority = MonitorPriority.Low,
                MonitorType = MonitorType.General,
                MonitorSource = MonitorSource.Central,
                Attachments = new[] {new MonitorAttachmentDto {AttachmentType = 0}}
            };
            var jDto = JsonConvert.SerializeObject(dto);
            var byteDto = Encoding.UTF8.GetBytes(jDto);
            var sBusMsg = new Message(byteDto);

            var mapAction = new Action(() => _mapper.Map(sBusMsg));

            // act
            // assert
            Assert.Throws<MessageCantBeMappedException>(mapAction);
        }

        [Fact]
        public void InvalidMessageStructure_Throws_Test()
        {
            // arrange
            const string jDto = @"{{}";
            var byteDto = Encoding.UTF8.GetBytes(jDto);
            var sBusMsg = new Message(byteDto);

            var mapAction = new Action(() => _mapper.Map(sBusMsg));

            // act
            // assert
            Assert.Throws<MessageCantBeMappedException>(mapAction);
        }

        [Theory]
        [ClassData(typeof(NullMessagesClassData))]
        public void NullMessage_Throws_Test(Message message)
        {
            // arrange
            var mapAction = new Action(()=> _mapper.Map(message));

            // act
            // assert
            Assert.Throws<ArgumentNullException>(mapAction);
        }
    }
}
