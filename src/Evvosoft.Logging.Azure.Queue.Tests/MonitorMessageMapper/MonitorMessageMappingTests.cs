using Evvosoft.Logging.Azure.Shared.Model.Mapping;
using Evvosoft.Logging.Azure.Shared.Model.Logging;
using NLog;
using NLog.Common;
using System;
using Xunit;

namespace Evvosoft.Logging.Azure.Queue.Tests.MonitorMessageMapper
{
    public class MonitorMessageMappingTests
    {
        [Fact]
        public void InvalidContent_LogEvent_IsNull_Test()
        {
            AsyncLogEventInfo logInfo = default(AsyncLogEventInfo);

            var mapAction = new Action(() => MonitorMessageMapping.Map(logInfo));

            Assert.Throws<ArgumentException>(mapAction);
        }

        [Fact]
        public void CorrectMessage_MappingByParameters_Test()
        {
            var now = DateTime.UtcNow;
            var dto = new MonitorMessageDto
            {
                Date = now,
                Priority = MonitorPriority.Low,
                MonitorType = MonitorType.General,
                MonitorSource = MonitorSource.Central
            };

            LogEventInfo info = LogEventInfo.CreateNullEvent();
            info.Parameters = new object[] { dto };

            AsyncLogEventInfo logInfo = new AsyncLogEventInfo(info, null);

            var mapped = MonitorMessageMapping.Map(logInfo);

            Assert.NotNull(mapped);
            Assert.Equal(dto, mapped);
        }

        [Fact]
        public void CorrectMessage_MappingWithDescription_Test()
        {
            LogEventInfo info = LogEventInfo.CreateNullEvent();
            info.Exception = new Exception("Test error");

            AsyncLogEventInfo logInfo = new AsyncLogEventInfo(info, null);

            var mapped = MonitorMessageMapping.Map(logInfo);

            Assert.NotNull(mapped);
            Assert.NotNull(mapped.Description);
            Assert.Equal("System.Exception: Test error", mapped.Description);
        }

        [Fact]
        public void CorrectMessage_MappingWithRealException_Test()
        {
            var now = DateTime.UtcNow;

            Exception source = new Exception("Test error");
            try
            {
                System.Runtime.ExceptionServices.ExceptionDispatchInfo.Throw(source);
            }
            catch (Exception exc)
            {
                source = exc;
            }

            System.Runtime.ExceptionServices.ExceptionDispatchInfo dInfo = System.Runtime.ExceptionServices.ExceptionDispatchInfo.Capture(source);
            LogEventInfo info = LogEventInfo.CreateNullEvent();
            info.Exception = dInfo.SourceException;
            info.Level = LogLevel.Error;
            info.TimeStamp = now;
            info.Message = "Event message";

            AsyncLogEventInfo logInfo = new AsyncLogEventInfo(info, null);

            var mapped = MonitorMessageMapping.Map(logInfo);

            Assert.NotNull(mapped);
            Assert.NotNull(mapped.StackTrace);
            Assert.NotNull(mapped.Message);
            Assert.Equal(MonitorPriority.Major, mapped.Priority);
            Assert.Equal(now, mapped.Date);
        }
    }
}
