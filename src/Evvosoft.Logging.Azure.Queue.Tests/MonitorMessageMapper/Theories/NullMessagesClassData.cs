using System.Collections;
using System.Collections.Generic;
using Microsoft.Azure.ServiceBus;

namespace Evvosoft.Logging.Azure.Queue.Tests.MonitorMessageMapper.Theories
{
    public class NullMessagesClassData : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[] {null};
            yield return new object[] {new Message(null)};
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
