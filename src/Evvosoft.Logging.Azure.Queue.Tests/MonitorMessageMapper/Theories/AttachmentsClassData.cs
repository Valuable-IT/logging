using Evvosoft.Logging.Azure.Shared.Model.Logging;
using System.Collections;
using System.Collections.Generic;

namespace Evvosoft.Logging.Azure.Queue.Tests.MonitorMessageMapper.Theories
{
    public class AttachmentsClassData : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[] {null};
            yield return new object[] {new MonitorAttachmentDto[] { }};
            yield return new object[] {new[] {new MonitorAttachmentDto {AttachmentType = AttachmentTypeDto.Url}}};
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
