using Evvosoft.Logging.Azure.Queue.Function.Logic.Service;
using Evvosoft.Logging.Azure.Queue.Function.Logic.Service.Data;
using Evvosoft.Logging.Azure.Queue.Function.Logic.Service.Interfaces;
using Evvosoft.Logging.DataLayer.Interface;
using Microsoft.Azure.ServiceBus.Core;
using Microsoft.Extensions.Logging;
using Moq;
using System.Threading;
using Xunit;

namespace Evvosoft.Logging.Azure.Queue.Tests.ReceivingService
{
    public class ReceivingCycleServiceTest
    {
        private const int BatchSize = 10;

        private readonly Mock<IPollQueueAndProcessService> _pollMock;
        private readonly IReceivingCycleService _rcvSvc;

        public ReceivingCycleServiceTest()
        {
            var msgRcvMock = new Mock<IMessageReceiver>();
            _pollMock = new Mock<IPollQueueAndProcessService>();
            var eStorageMock = new Mock<IMonitorEventStorage>();
            var logMock = new Mock<ILogger>();

            _rcvSvc = new ReceivingCycleService(
                msgRcvMock.Object,
                _pollMock.Object,
                eStorageMock.Object,
                logMock.Object);
        }

        [Fact]
        public async void ForEmptyBatch_Return_Done()
        {
            var result = await _rcvSvc.RunCycle(0, CancellationToken.None);
            Assert.Equal(CycleServiceResultType.AllDone, result);
        }

        [Fact]
        public async void ForZeroMessages_Terminates()
        {
            // arrange
            _pollMock.Setup(x => x.PollQueueAndProcess(
                It.IsAny<IMessageReceiver>(),
                It.IsAny<IMonitorEventStorage>(),
                It.IsAny<ILogger>(),
                It.IsAny<CancellationToken>())).ReturnsAsync(0);

            // act
            var result = await _rcvSvc.RunCycle(BatchSize, CancellationToken.None);

            // assert
            _pollMock.Verify(x => x.PollQueueAndProcess(It.IsAny<IMessageReceiver>(),
                It.IsAny<IMonitorEventStorage>(),
                It.IsAny<ILogger>(),
                It.IsAny<CancellationToken>()), Times.Once);

            Assert.Equal(CycleServiceResultType.TooFewMessages, result);
        }

        [Fact]
        public async void ForOneMessage_Terminates()
        {
            // arrange
            _pollMock.Setup(x => x.PollQueueAndProcess(
                It.IsAny<IMessageReceiver>(),
                It.IsAny<IMonitorEventStorage>(),
                It.IsAny<ILogger>(),
                It.IsAny<CancellationToken>())).ReturnsAsync(1);

            // act
            var result = await _rcvSvc.RunCycle(BatchSize, CancellationToken.None);

            // assert
            _pollMock.Verify(x => x.PollQueueAndProcess(It.IsAny<IMessageReceiver>(),
                It.IsAny<IMonitorEventStorage>(),
                It.IsAny<ILogger>(),
                It.IsAny<CancellationToken>()), Times.Once);

            Assert.Equal(CycleServiceResultType.TooFewMessages, result);
        }

        [Fact]
        public async void ForSeveralMessage_Success()
        {
            // arrange
            _pollMock.Setup(x => x.PollQueueAndProcess(
                It.IsAny<IMessageReceiver>(),
                It.IsAny<IMonitorEventStorage>(),
                It.IsAny<ILogger>(),
                It.IsAny<CancellationToken>())).ReturnsAsync(10);

            // act
            var result = await _rcvSvc.RunCycle(BatchSize, CancellationToken.None);

            // assert
            _pollMock.Verify(x => x.PollQueueAndProcess(It.IsAny<IMessageReceiver>(),
                It.IsAny<IMonitorEventStorage>(),
                It.IsAny<ILogger>(),
                It.IsAny<CancellationToken>()), Times.Exactly(BatchSize));

            Assert.Equal(CycleServiceResultType.AllDone, result);
        }
    }
}
