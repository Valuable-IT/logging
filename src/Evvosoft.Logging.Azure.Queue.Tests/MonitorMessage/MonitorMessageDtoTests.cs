using System;
using System.Reflection;
using Evvosoft.Logging.Azure.Queue.Tests.Base;
using Evvosoft.Logging.Azure.Shared.Model.Logging;
using Newtonsoft.Json;
using Xunit;

namespace Evvosoft.Logging.Azure.Queue.Tests.MonitorMessage
{
    public class MonitorMessageDtoTests : MonitorMessageDtoTestsBase
    {
        private readonly string _okMessage;
        private readonly string _okMsgWithAtt;
        private readonly string _incompleteMessage;

        public MonitorMessageDtoTests()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var okMsgResource = $"{assembly.GetName().Name}.MonitorMessage.Messages.fullOkMsg.json";
            var fullOkMsgWithAttResource = $"{assembly.GetName().Name}.MonitorMessage.Messages.fullOkMsgWithAtt.json";
            var incompleteMsgResource = $"{assembly.GetName().Name}.MonitorMessage.Messages.incompleteMsg.json";

            _okMessage = ReadResourceContent(okMsgResource, assembly);
            _okMsgWithAtt = ReadResourceContent(fullOkMsgWithAttResource, assembly);
            _incompleteMessage = ReadResourceContent(incompleteMsgResource, assembly);
        }

        [Fact]
        public void CorrectPayload_Should_Be_Deserialized_Test()
        {
            var messageDto = JsonConvert.DeserializeObject<MonitorMessageDto>(_okMessage);

            Assert.NotNull(messageDto);
            Assert.Equal("test Msg", messageDto.Message);
            Assert.Equal("test Description", messageDto.Description);
            Assert.Equal("test StackTrace", messageDto.StackTrace);
            Assert.Equal(MonitorPriority.Medium, messageDto.Priority);
            Assert.Equal(MonitorType.General, messageDto.MonitorType);
            Assert.Equal(MonitorSource.Central, messageDto.MonitorSource);

            var expDt = new DateTime(2019, 1, 1, 12, 15, 45);
            expDt = DateTime.SpecifyKind(expDt, DateTimeKind.Utc);
            Assert.Equal(expDt, messageDto.Date);
            Assert.Null(messageDto.Attachments);
        }

        [Fact]
        public void CorrectPayloadWithAtt_Should_Be_Deserialized_Test()
        {
            var messageDto = JsonConvert.DeserializeObject<MonitorMessageDto>(_okMsgWithAtt);

            Assert.NotNull(messageDto);
            Assert.Equal("test Msg", messageDto.Message);
            Assert.Equal("test Description", messageDto.Description);
            Assert.Equal("test StackTrace", messageDto.StackTrace);
            Assert.Equal(MonitorPriority.Medium, messageDto.Priority);
            Assert.Equal(MonitorType.General, messageDto.MonitorType);
            Assert.Equal(MonitorSource.Central, messageDto.MonitorSource);

            var expDt = new DateTime(2019, 1, 1, 12, 15, 45);
            expDt = DateTime.SpecifyKind(expDt, DateTimeKind.Utc);
            Assert.Equal(expDt, messageDto.Date);

            // attachments
            Assert.NotNull(messageDto.Attachments);
            Assert.Equal(2, messageDto.Attachments.Length);

            Assert.Equal(AttachmentTypeDto.Url, messageDto.Attachments[0].AttachmentType);
            Assert.Equal("http:\\test.com", messageDto.Attachments[0].AttachmentData);

            Assert.Equal(AttachmentTypeDto.WebLink, messageDto.Attachments[1].AttachmentType);
            Assert.Equal("http:\\test2.com", messageDto.Attachments[1].AttachmentData);
        }

        [Fact]
        public void IncompletePayload_Should_Be_Deserialized_Test()
        {
            var messageDto = JsonConvert.DeserializeObject<MonitorMessageDto>(_incompleteMessage);

            Assert.NotNull(messageDto);
            Assert.Equal("test Msg", messageDto.Message);
            Assert.Null(messageDto.Description);
            Assert.Null(messageDto.StackTrace);
            Assert.Equal(MonitorPriority.Undefined, messageDto.Priority);
            Assert.Equal(0, (int)messageDto.MonitorType);
            Assert.Equal(0, (int)messageDto.MonitorSource);

            var expDt = new DateTime(2019, 1, 1, 12, 15, 45);
            expDt = DateTime.SpecifyKind(expDt, DateTimeKind.Utc);
            Assert.Equal(expDt, messageDto.Date);
            Assert.Null(messageDto.Attachments);
        }
    }
}
