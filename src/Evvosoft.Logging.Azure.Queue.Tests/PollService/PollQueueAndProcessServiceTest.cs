using Evvosoft.Logging.Azure.Queue.Function.Logic.Model.Mapper.Exceptions;
using Evvosoft.Logging.Azure.Queue.Function.Logic.Model.Mapper.Interfaces;
using Evvosoft.Logging.Azure.Queue.Function.Logic.Service;
using Evvosoft.Logging.Azure.Queue.Function.Logic.Service.Interfaces;
using Evvosoft.Logging.Azure.Shared.Model.Logging;
using Evvosoft.Logging.DataLayer.Interface;
using Microsoft.Azure.ServiceBus;
using Microsoft.Azure.ServiceBus.Core;
using Microsoft.Extensions.Logging;
using Moq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Xunit;

namespace Evvosoft.Logging.Azure.Queue.Tests.PollService
{
    public class PollQueueAndProcessServiceTest
    {
        private const int MsqInBatch = 10;
        private const int MsgQueueTestTimeoutInMSec = 100;
        private const int MsqBatchFormingTimeout = 5;

        private readonly Mock<IMessageReceiver> _msgRcvMock;
        private readonly Mock<IMonitorEventStorage> _storageMock;
        private readonly Mock<IMessageMapper> _mapperMock;
        private readonly Mock<ILogger> _logMock;

        private readonly IPollQueueAndProcessService _pollSvc;

        public PollQueueAndProcessServiceTest()
        {
            _msgRcvMock = new Mock<IMessageReceiver>();
            _storageMock = new Mock<IMonitorEventStorage>();
            _mapperMock = new Mock<IMessageMapper>();
            _logMock = new Mock<ILogger>();

            _pollSvc = new PollQueueAndProcessService(
                _mapperMock.Object,
                MsqInBatch,
                MsgQueueTestTimeoutInMSec,
                MsqBatchFormingTimeout);
        }

        [Fact]
        public void CtorThrows_When_MapperNull()
        {
            var ctor = new Action(() => { new PollQueueAndProcessService(null, 1, 1, 1); });
            Assert.Throws<ArgumentNullException>(ctor);
        }

        [Fact]
        public async void ServiceReturns_Zero_If_EmptyOrNullReceived()
        {
            //arrange
            _msgRcvMock.Setup(x => x.ReceiveAsync(It.IsAny<TimeSpan>()))
                .ReturnsAsync((Message)null);

            //act
            var msgCnt = await _pollSvc.PollQueueAndProcess(_msgRcvMock.Object, null, null, CancellationToken.None);

            //assert
            Assert.Equal(0, msgCnt);
            _msgRcvMock.Verify(x => x.ReceiveAsync(It.IsAny<TimeSpan>()),
                Times.Once);
        }

        [Fact]
        public async void ServiceProcesses_OneMessage_Success_Scenario()
        {
            //arrange
            var dtNow = DateTime.UtcNow;
            var dto = new MonitorMessageDto
            {
                Message = "test",
                Date = dtNow,
                Priority = MonitorPriority.Low,
                MonitorType =  MonitorType.Central
            };
            var jDto = JsonConvert.SerializeObject(dto);
            var byteDto = Encoding.UTF8.GetBytes(jDto);
            var lockToken = Guid.NewGuid();
            var sBusMsg = new CustomMessage(byteDto, lockToken);

            _msgRcvMock.Setup(x => x.ReceiveAsync(It.IsAny<TimeSpan>()))
                .ReturnsAsync(sBusMsg);

            _msgRcvMock.Setup(x => x.ReceiveAsync(
                    It.IsAny<int>(),
                    It.IsAny<TimeSpan>()))
                .ReturnsAsync((List<Message>) null);

            _mapperMock.Setup(x => x.Map(It.IsAny<Message>()))
                .Returns(dto);

            var ct = CancellationToken.None;

            //act
            var msgCnt = await _pollSvc.PollQueueAndProcess(
                _msgRcvMock.Object,
                _storageMock.Object,
                _logMock.Object,
                ct);

            //assert
            Assert.Equal(1, msgCnt);

            // one was saved to DB
            _storageMock.Verify(x =>
                x.InsertMessages(It.Is<IList<MonitorMessageDto>>(m =>
                        m.SequenceEqual(new List<MonitorMessageDto> { dto })),
                    It.Is<CancellationToken>(c => c == ct)), Times.Once);

            // one was committed
            _msgRcvMock.Verify(
                x => x.CompleteAsync(It.Is<IEnumerable<string>>(l =>
                    l.SequenceEqual(new List<string> { lockToken.ToString() }))), Times.Once);
        }

        [Fact]
        public async void ServiceProcesses_SeveralMessages_Success_Scenario()
        {
            //arrange
            var dtNow = DateTime.UtcNow;
            var dto = new MonitorMessageDto
            {
                Message = "test",
                Date = dtNow,
                Priority = MonitorPriority.Low,
                MonitorType =  MonitorType.Central
            };
            var jDto = JsonConvert.SerializeObject(dto);
            var byteDto = Encoding.UTF8.GetBytes(jDto);
            var lockToken = Guid.NewGuid();
            var sBusMsg = new CustomMessage(byteDto, lockToken);

            var dto2 = new MonitorMessageDto
            {
                Message = "test2",
                Date = dtNow,
                Priority = MonitorPriority.Medium,
                MonitorType = MonitorType.Central
            };
            var jDto2 = JsonConvert.SerializeObject(dto2);
            var byteDto2 = Encoding.UTF8.GetBytes(jDto2);
            var lockToken2 = Guid.NewGuid();
            var sBusMsg2 = new CustomMessage(byteDto2, lockToken2);

            _msgRcvMock.Setup(x => x.ReceiveAsync(It.IsAny<TimeSpan>()))
                .ReturnsAsync(sBusMsg);

            _msgRcvMock.Setup(x => x.ReceiveAsync(
                    It.IsAny<int>(),
                    It.IsAny<TimeSpan>()))
                .ReturnsAsync(new List<Message> {sBusMsg2});

            _mapperMock.Setup(x => x.Map(It.Is<Message>(m=>m.SystemProperties.LockToken == sBusMsg.SystemProperties.LockToken)))
                .Returns(dto);

            _mapperMock.Setup(x => x.Map(It.Is<Message>(m => m.SystemProperties.LockToken == sBusMsg2.SystemProperties.LockToken)))
                .Returns(dto2);

            var ct = CancellationToken.None;

            //act
            var msgCnt = await _pollSvc.PollQueueAndProcess(
                _msgRcvMock.Object,
                _storageMock.Object,
                _logMock.Object,
                ct);

            //assert
            Assert.Equal(2, msgCnt);

            // One message receive was called
            _msgRcvMock.Verify(x => x.ReceiveAsync(It.Is<TimeSpan>(t => t == TimeSpan.FromMilliseconds(MsgQueueTestTimeoutInMSec))),
                Times.Once);

            // Batch size is one less, timeout is correct
            _msgRcvMock.Verify(x => x.ReceiveAsync(It.Is<int>(v => v == MsqInBatch - 1),
                    It.Is<TimeSpan>(t => t == TimeSpan.FromSeconds(MsqBatchFormingTimeout))),
                Times.Once);

            // two were saved to DB
            _storageMock.Verify(x =>
                x.InsertMessages(It.Is<IList<MonitorMessageDto>>(m =>
                        m.SequenceEqual(new List<MonitorMessageDto> { dto2, dto })),
                    It.Is<CancellationToken>(c => c == ct)), Times.Once);

            // two were committed
            _msgRcvMock.Verify(
                x => x.CompleteAsync(It.Is<IEnumerable<string>>(l =>
                    l.SequenceEqual(new List<string> {lockToken2.ToString(), lockToken.ToString()}))), Times.Once);
        }

       
        [Fact]
        public async void ServiceProcesses_CorruptedMessage_Scenario()
        {
            //arrange
            var dtNow = DateTime.UtcNow;
            var dto = new MonitorMessageDto
            {
                Message = "test",
                Date = dtNow,
                Priority = MonitorPriority.Low,
                MonitorType =  MonitorType.General
            };
            var jDto = JsonConvert.SerializeObject(dto);
            var byteDto = Encoding.UTF8.GetBytes(jDto);
            var lockToken = Guid.NewGuid();
            var sBusMsg = new CustomMessage(byteDto, lockToken);

            _msgRcvMock.Setup(x => x.ReceiveAsync(It.IsAny<TimeSpan>()))
                .ReturnsAsync(sBusMsg);

            _msgRcvMock.Setup(x => x.ReceiveAsync(
                    It.IsAny<int>(),
                    It.IsAny<TimeSpan>()))
                .ReturnsAsync((List<Message>)null);

            _mapperMock.Setup(x => x.Map(It.IsAny<Message>()))
                .Throws(new MessageCantBeMappedException("payload", new Exception("smth")));

            var ct = CancellationToken.None;

            //act
            var msgCnt = await _pollSvc.PollQueueAndProcess(
                _msgRcvMock.Object,
                _storageMock.Object,
                _logMock.Object,
                ct);

            //assert
            Assert.Equal(0, msgCnt);

            // DB wasn't called
            _storageMock.Verify(x =>
                x.InsertMessages(It.IsAny<IList<MonitorMessageDto>>(),
                    It.IsAny<CancellationToken>()), Times.Never);

            // one was dead lettered
            _msgRcvMock.Verify(
                x => x.DeadLetterAsync(
                    It.Is<string>(l => l == lockToken.ToString()),
                    It.IsAny<IDictionary<string, object>>()), Times.Once);
        }
    }

    /// <summary>
    /// In order to set system properties
    /// </summary>
    class CustomMessage : Message
    {
        private static readonly Type SpType = typeof(SystemPropertiesCollection);

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="body">Message body</param>
        /// <param name="lockToken">Lock token to verify</param>
        public CustomMessage(byte[] body, Guid lockToken) : base(body)
        {
            // HACK properties to make tests possible. Sequence number needs to read system properties
            var sqProp = SpType.GetField("sequenceNumber", System.Reflection.BindingFlags.NonPublic
                                                           | System.Reflection.BindingFlags.Instance);
            var lockProp = SpType.GetField("lockTokenGuid", System.Reflection.BindingFlags.NonPublic
                                                            | System.Reflection.BindingFlags.Instance);

            sqProp.SetValue(SystemProperties, 100500);
            lockProp.SetValue(SystemProperties, lockToken);
        }
    }
}
