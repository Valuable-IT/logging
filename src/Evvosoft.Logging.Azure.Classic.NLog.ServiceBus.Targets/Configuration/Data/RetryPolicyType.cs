namespace Evvosoft.Logging.Azure.Classic.NLog.ServiceBus.Targets.Configuration.Data
{
    public enum RetryPolicyType
    {
        NoRetry = 0,
        Default = 1
    }
}
