using System.Collections.Generic;
using Microsoft.Azure.ServiceBus;

namespace Evvosoft.Logging.Azure.Classic.NLog.ServiceBus.Targets.MessagesPortion.Interfaces
{
    public interface IMessagesPortionFormer
    {
        /// <summary>
        /// Takes portion according to maxPortionInBytes size of messages skipping skipN elements
        /// </summary>
        /// <param name="messages">Messages set to process</param>
        /// <param name="maxPortionInBytes">Max portion size in bytes to form</param>
        /// <param name="skipN">Skips first N elements before starting portion forming</param>
        /// <returns>Messages portion</returns>
        IEnumerable<Message> TakePortion(IEnumerable<Message> messages, long maxPortionInBytes, int skipN = 0);
    }
}
