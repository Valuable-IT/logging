using Evvosoft.Logging.Azure.Queue.Function.Logic.Service.Data;
using Evvosoft.Logging.Azure.Queue.Function.Logic.Service.Interfaces;
using Evvosoft.Logging.DataLayer.Interface;
using Microsoft.Azure.ServiceBus.Core;
using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;

namespace Evvosoft.Logging.Azure.Queue.Function.Logic.Service
{
    public class ReceivingCycleService : IReceivingCycleService
    {
        private readonly IMessageReceiver _msgRcv;
        private readonly IPollQueueAndProcessService _processService;
        private readonly IMonitorEventStorage _eStorage;
        private readonly ILogger _log;

        public ReceivingCycleService(
            IMessageReceiver msgRcv,
            IPollQueueAndProcessService processService,
            IMonitorEventStorage eStorage,
            ILogger log)
        {
            _msgRcv = msgRcv;
            _processService = processService;
            _eStorage = eStorage;
            _log = log;
        }

        public async Task<CycleServiceResultType> RunCycle(int maxBatches, CancellationToken ct)
        {
            var msgBatchLeft = maxBatches;

            // Do batch processing in loop
            while (msgBatchLeft-- > 0)
            {
                var msgCnt = await _processService.PollQueueAndProcess(_msgRcv, _eStorage, _log, ct);

                // Terminate function if noting to process, or just one message was processed
                if (0 == msgCnt || 1 == msgCnt)
                {
                    // if the cycle is first
                    return maxBatches == msgBatchLeft + 1
                        ? CycleServiceResultType.TooFewMessages
                        // At least one batch was processed
                        : CycleServiceResultType.AllDone;
                }
            }

            return CycleServiceResultType.AllDone;
        }
    }
}
