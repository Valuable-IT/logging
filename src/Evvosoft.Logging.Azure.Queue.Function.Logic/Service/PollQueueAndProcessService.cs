using Evvosoft.Logging.Azure.Queue.Function.Logic.Model.Mapper.Interfaces;
using Evvosoft.Logging.Azure.Queue.Function.Logic.Service.Interfaces;
using Evvosoft.Logging.Azure.Shared.Model.Logging;
using Evvosoft.Logging.DataLayer.Interface;
using Microsoft.Azure.ServiceBus;
using Microsoft.Azure.ServiceBus.Core;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Evvosoft.Logging.Azure.Queue.Function.Logic.Service
{
    public class PollQueueAndProcessService : IPollQueueAndProcessService
    {
        private readonly IMessageMapper _mapper;
        private readonly int _msgInBatch;
        private readonly TimeSpan _msgQueueTestReceiveTimeoutMSec;
        private readonly TimeSpan _msgBatchFormTimeoutSec;

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="mapper">Message to DTO mapper</param>
        /// <param name="msgInBatch">Messages in batch</param>
        /// <param name="msgTestReceiveTimeoutMSec">Trying to receive first message to check queue in mSeconds</param>
        /// <param name="msgBatchFormTimeoutSec">Batch forming timeout in seconds</param>
        public PollQueueAndProcessService(
            IMessageMapper mapper,
            int msgInBatch,
            int msgTestReceiveTimeoutMSec,
            int msgBatchFormTimeoutSec)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _msgInBatch = msgInBatch;
            _msgQueueTestReceiveTimeoutMSec = TimeSpan.FromMilliseconds(msgTestReceiveTimeoutMSec);
            _msgBatchFormTimeoutSec = TimeSpan.FromSeconds(msgBatchFormTimeoutSec);
        }

        public async Task<int> PollQueueAndProcess(
            IMessageReceiver msgRcv,
            IMonitorEventStorage eStorage,
            ILogger log,
            CancellationToken ct)
        {
            // Cost optimization: In order not to wait too long, just check queue quickly before waiting for the batch
            var oneMsg = await msgRcv.ReceiveAsync(_msgQueueTestReceiveTimeoutMSec);
            if (null == oneMsg)
            {
                return 0;
            }

            // Try get 'MessagesInBatch' messages within 'MessagesBatchFormingTimeoutInSec' timeout
            var msgSet = await msgRcv.ReceiveAsync(
                             _msgInBatch - 1,
                             _msgBatchFormTimeoutSec)
                         ?? new List<Message>();
            // Not to forget that one message to add
            msgSet.Add(oneMsg);

            var msgSetToStore = new List<Tuple<Message, MonitorMessageDto>>();
            foreach (var message in msgSet)
            {
                try
                {
                    // Try to map payload
                    var dto = _mapper.Map(message);
                    msgSetToStore.Add(new Tuple<Message, MonitorMessageDto>(message, dto));
                }
                catch (Exception e)
                {
                    // In case of any mapping errors the message will be sent to the DLQ
                    log.LogError(e, $"{e.Message}");
                    await msgRcv.DeadLetterAsync(message.SystemProperties.LockToken);
                }
            }

            // If all messages are corrupted and validation was failed
            if (!msgSetToStore.Any())
                return 0;

            // Having bad messages filtered out and valid mapped do batch saving
            var msgToInsert = msgSetToStore.Select(x => x.Item2).ToList();
            await eStorage.InsertMessages(msgToInsert, ct);
            log.LogInformation($"'{msgToInsert.Count}' of '{msgSet.Count}' messages were inserted into DB");

            // complete locked messages
            var msgTokens = msgSetToStore.Select(x => x.Item1.SystemProperties.LockToken);
            await msgRcv.CompleteAsync(msgTokens);

            log.LogInformation("Batch has been finished successfully");
            return msgToInsert.Count;
        }
    }
}
