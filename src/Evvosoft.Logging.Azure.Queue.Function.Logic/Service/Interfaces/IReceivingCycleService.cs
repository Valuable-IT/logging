using System.Threading;
using System.Threading.Tasks;
using Evvosoft.Logging.Azure.Queue.Function.Logic.Service.Data;

namespace Evvosoft.Logging.Azure.Queue.Function.Logic.Service.Interfaces
{
    public interface IReceivingCycleService
    {
        Task<CycleServiceResultType> RunCycle(int maxBatches, CancellationToken ct);
    }
}
