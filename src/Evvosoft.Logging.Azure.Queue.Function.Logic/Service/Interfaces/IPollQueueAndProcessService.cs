using Evvosoft.Logging.DataLayer.Interface;
using Microsoft.Azure.ServiceBus.Core;
using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;

namespace Evvosoft.Logging.Azure.Queue.Function.Logic.Service.Interfaces
{
    public interface IPollQueueAndProcessService
    {
        /// <summary>
        /// Pulls messages from queue and sends them to SQL DB
        /// </summary>
        /// <param name="msgRcv">Service Bus messages receiver</param>
        /// <param name="eStorage">SQL DB storage client</param>
        /// <param name="log">Logger</param>
        /// <param name="ct">Cancellation token</param>
        /// <returns>Messages count which were processed</returns>
        Task<int> PollQueueAndProcess(
            IMessageReceiver msgRcv,
            IMonitorEventStorage eStorage,
            ILogger log,
            CancellationToken ct);
    }
}
