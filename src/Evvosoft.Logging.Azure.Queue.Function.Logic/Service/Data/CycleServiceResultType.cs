namespace Evvosoft.Logging.Azure.Queue.Function.Logic.Service.Data
{
    public enum CycleServiceResultType
    {
        AllDone = 0,
        TooFewMessages = 1
    }
}
