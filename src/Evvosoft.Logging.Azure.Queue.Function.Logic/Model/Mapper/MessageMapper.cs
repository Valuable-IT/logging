using System;
using System.Text;
using Evvosoft.Logging.Azure.Queue.Function.Logic.Model.Mapper.Exceptions;
using Evvosoft.Logging.Azure.Queue.Function.Logic.Model.Mapper.Interfaces;
using Evvosoft.Logging.Azure.Shared.Model.Logging;
using Microsoft.Azure.ServiceBus;
using Newtonsoft.Json;

namespace Evvosoft.Logging.Azure.Queue.Function.Logic.Model.Mapper
{
    public class MessageMapper : IMessageMapper
    {
        // Name field limit in DB
        private const int MaxNameLength = 250;
        private readonly TimeZoneInfo estTimeZoneInfo;

        public MessageMapper()
        {
            estTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
        }

        public MonitorMessageDto Map(Message message)
        {
            if (null == message)
                throw new ArgumentNullException(nameof(message));

            if (null == message.Body)
                throw new ArgumentNullException(nameof(message.Body));

            string jBody = null;

            try
            {
                jBody = Encoding.UTF8.GetString(message.Body);
                var msgDto = JsonConvert.DeserializeObject<MonitorMessageDto>(jBody);
                AssertMessage(msgDto);

                var utc = TimeZoneInfo.ConvertTimeToUtc(msgDto.Date);
                msgDto.Date = TimeZoneInfo.ConvertTime(utc, estTimeZoneInfo);

                // cut excess message body
                if (msgDto.Message.Length > MaxNameLength)
                {
                    msgDto.Message = msgDto.Message.Substring(0, MaxNameLength);
                }

                return msgDto;
            }
            catch (Exception e)
            {
                throw new MessageCantBeMappedException(jBody, e);
            }
        }

        /// <summary>
        /// Checks message structure, throws if something wrong and unsuitable for DB
        /// </summary>
        private void AssertMessage(MonitorMessageDto message)
        {
            if(string.IsNullOrEmpty(message.Message))
                throw new ArgumentNullException(nameof(message.Message));

            if (default(DateTime) == message.Date)
                throw new ArgumentOutOfRangeException(nameof(message.Date));

            if (0 == message.Priority)
                throw new ArgumentOutOfRangeException(nameof(message.Priority),
                    message.Priority,
                    "MonitorPriorityId is predefined in DB");

            if (0 == (int)message.MonitorType)
                throw new ArgumentOutOfRangeException(nameof(message.MonitorType),
                    (int)message.MonitorType,
                    "MonitorTypeId is predefined in DB");

            if (0 == (int)message.MonitorSource)
                throw new ArgumentOutOfRangeException(nameof(message.MonitorSource),
                    (int)message.MonitorSource,
                    "MonitorSourceId is predefined in DB");

            if (null != message.Attachments)
            {
                foreach (var attachment in message.Attachments)
                {
                    if (0 == attachment.AttachmentType)
                        throw new ArgumentOutOfRangeException(nameof(attachment.AttachmentType),
                            attachment.AttachmentType,
                            "AttachmentType is predefined in DB");
                }
            }
        }
    }
}
