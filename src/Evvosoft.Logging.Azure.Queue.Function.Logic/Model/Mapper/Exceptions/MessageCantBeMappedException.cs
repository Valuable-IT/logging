using System;
using Evvosoft.Logging.Azure.Shared.Model.Logging;

namespace Evvosoft.Logging.Azure.Queue.Function.Logic.Model.Mapper.Exceptions
{
    public class MessageCantBeMappedException : Exception
    {
        public MessageCantBeMappedException(string payload, Exception innerException) : base(
            $"Message payload: '{payload}' can't be mapped to the DTO of type {typeof(MonitorMessageDto)}",
            innerException)
        {
        }
    }
}
