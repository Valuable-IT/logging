using Evvosoft.Logging.Azure.Shared.Model.Logging;
using Microsoft.Azure.ServiceBus;

namespace Evvosoft.Logging.Azure.Queue.Function.Logic.Model.Mapper.Interfaces
{
    public interface IMessageMapper
    {
        /// <summary>
        /// Maps input service bus message to Monitor message DTO format
        /// </summary>
        /// <param name="message">Input service bus message</param>
        /// <returns>Monitor DTO message</returns>
        MonitorMessageDto Map(Message message);
    }
}
