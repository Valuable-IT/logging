using System;
using System.Collections.Generic;
using System.Linq;
using Evvosoft.Logging.Azure.NLog.ServiceBus.Interfaces.Mapping;
using Evvosoft.Logging.Azure.NLog.ServiceBus.Interfaces.Mapping.Exceptions;
using Evvosoft.Logging.Azure.NLog.ServiceBus.Interfaces.Mapping.Strategies;
using Microsoft.Azure.ServiceBus;
using NLog.Common;

namespace Evvosoft.Logging.Azure.NLog.ServiceBus.Targets.Mapping
{
    public class LogEventToMessageMapper : ILogEventToMessageMapper
    {
        private  readonly IEnumerable<IMessageMappingStrategy> _strategiesSet;
        private readonly TimeSpan _msgTimeToLive;

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="mappingStrategies">Message mapping strategies</param>
        /// <param name="msgTimeToLive">Default time to live for message</param>
        public LogEventToMessageMapper(IEnumerable<IMessageMappingStrategy> mappingStrategies, TimeSpan msgTimeToLive)
        {
            _strategiesSet = mappingStrategies ?? throw new ArgumentNullException(nameof(mappingStrategies));
            _msgTimeToLive = msgTimeToLive;
        }

        public Message Map(AsyncLogEventInfo logInfo)
        {
            var mapper = _strategiesSet.FirstOrDefault(x => x.CanMap(logInfo));

            if (null != mapper)
            {
                var msg = mapper.Map(logInfo);
                msg.TimeToLive = _msgTimeToLive;
                return msg;
            }

            throw new EventCantBeMappedException(
                $"There is no mapping strategy defined for Log event with message: '{logInfo.LogEvent.FormattedMessage}'");
        }
    }
}
