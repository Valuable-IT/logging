using Evvosoft.Logging.Azure.NLog.ServiceBus.Targets.Configuration.Data;

namespace Evvosoft.Logging.Azure.NLog.ServiceBus.Targets.Configuration.Interfaces
{
    public interface IAsbTargetConfiguration
    {
        /// <summary>
        /// Azure service bus connection string, "Endpoint=sb://service..."
        /// </summary>
        string ConnectionString { get; set; }

        /// <summary>
        /// Queue for outgoing messages
        /// </summary>
        string Queue { get; set; }

        /// <summary>
        /// Message sending retry policy
        /// </summary>
        RetryPolicyType RetryStrategyPolicy { get; set; }

        /// <summary>
        /// Message client timeout in seconds which required for messages sending
        /// </summary>
        int ClientTimeoutInSeconds { get; set; }

        /// <summary>
        /// Message time to live before it will be sent to DLQ
        /// </summary>
        int MessageTimeToLiveInHours { get; set; }

        /// <summary>
        /// Maximum message or batch size per one message sending call
        /// </summary>
        long MessageMaximumSizePerBatch { get; set; }

        /// <summary>
        /// Maximum message count per batch. This limit prevents sending client from delivery ack flood which causes message size exception
        /// </summary>
        int MessageMaximumCountPerBatch { get; set; }
    } 
}
