namespace Evvosoft.Logging.Azure.NLog.ServiceBus.Targets.Configuration.Data
{
    public enum RetryPolicyType
    {
        NoRetry = 0,
        Default = 1
    }
}
