using System;
using System.Collections.Generic;
using System.Linq;

namespace Evvosoft.Logging.Azure.NLog.ServiceBus.Targets.MessagesPortion
{
    public static class EnumerableExtensions
    {
        /// <summary>
        /// Takes something while predicate is true
        /// </summary>
        /// <typeparam name="TSource">Source element Type to iterate through</typeparam>
        /// <typeparam name="TAccumulate">Accumulator type to accumulate calculation results</typeparam>
        /// <param name="source">Source collection</param>
        /// <param name="seed">Initial accumulator seed</param>
        /// <param name="func">Calculation function</param>
        /// <param name="predicate">Calculation boundary predicate</param>
        /// <returns></returns>
        public static IEnumerable<TSource> TakeWhileAggregate<TSource, TAccumulate>(
            this IEnumerable<TSource> source,
            TAccumulate seed,
            Func<TAccumulate, TSource, TAccumulate> func,
            Func<TAccumulate, bool> predicate
        )
        {
            var accumulator = seed;
            foreach (var item in source)
            {
                accumulator = func(accumulator, item);
                if (predicate(accumulator))
                {
                    yield return item;
                }
                else
                {
                    yield break;
                }
            }
        }

        /// <summary>
        /// Splits input sequence into chunks
        /// </summary>
        /// <typeparam name="T">Element type stored in collection</typeparam>
        /// <param name="source">Source collection</param>
        /// <param name="itemsPerChunk">Items per chunk</param>
        /// <returns></returns>
        public static IEnumerable<IEnumerable<T>> SplitIntoChunks<T>
            (this IEnumerable<T> source, int itemsPerChunk)
        {
            var sourceList = source as List<T> ?? source.ToList();
            for (var index = 0; index < sourceList.Count; index += itemsPerChunk)
            {
                yield return sourceList.Skip(index).Take(itemsPerChunk);
            }
        }
    }
}
