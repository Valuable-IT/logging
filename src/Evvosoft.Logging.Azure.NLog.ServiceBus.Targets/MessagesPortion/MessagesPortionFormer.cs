using System;
using System.Collections.Generic;
using System.Linq;
using Evvosoft.Logging.Azure.NLog.ServiceBus.Targets.MessagesPortion.Interfaces;
using Microsoft.Azure.ServiceBus;

namespace Evvosoft.Logging.Azure.NLog.ServiceBus.Targets.MessagesPortion
{
    public class MessagesPortionFormer : IMessagesPortionFormer
    {
        public IEnumerable<Message> TakePortion(IEnumerable<Message> messages, long maxPortionInBytes, int skipN)
        {
            if (null == messages)
                throw new ArgumentNullException(nameof(messages));

            var portion = messages
                .Skip(skipN)
                .TakeWhileAggregate((long)0,
                    (sizeAcc, msg) => sizeAcc + msg.Size,
                    sizeAcc => sizeAcc <= maxPortionInBytes);

            return portion;
        }
    }
}
