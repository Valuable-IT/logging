using System;

namespace Evvosoft.Logging.Azure.NLog.ServiceBus.Targets.Exceptions
{
    /// <summary>
    /// Indicates that such huge message can't be sent via message bus
    /// </summary>
    public class MessageIsTooBigException : Exception
    {
        public MessageIsTooBigException(long size, long maxBytes) : base(
            $"{size} bytes in too big portion to be sent via Service Bus. Max size is {maxBytes} bytes.")
        {
        }
    }
}
