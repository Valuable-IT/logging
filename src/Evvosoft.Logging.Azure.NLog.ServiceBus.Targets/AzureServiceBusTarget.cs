using System;
using System.Collections.Generic;
using System.Linq;
using Evvosoft.Logging.Azure.NLog.ServiceBus.Interfaces.Mapping;
using Evvosoft.Logging.Azure.NLog.ServiceBus.Interfaces.Mapping.Strategies;
using Evvosoft.Logging.Azure.NLog.ServiceBus.Monitor.Mapping.Strategies;
using Evvosoft.Logging.Azure.NLog.ServiceBus.Targets.Configuration.Data;
using Evvosoft.Logging.Azure.NLog.ServiceBus.Targets.Configuration.Interfaces;
using Evvosoft.Logging.Azure.NLog.ServiceBus.Targets.Exceptions;
using Evvosoft.Logging.Azure.NLog.ServiceBus.Targets.Mapping;
using Evvosoft.Logging.Azure.NLog.ServiceBus.Targets.MessagesPortion;
using Evvosoft.Logging.Azure.NLog.ServiceBus.Targets.MessagesPortion.Interfaces;
using Microsoft.Azure.ServiceBus;
using Microsoft.Azure.ServiceBus.Core;
using NLog.Common;
using NLog.Config;
using NLog.Targets;

namespace Evvosoft.Logging.Azure.NLog.ServiceBus.Targets
{
    [Target("AzureServiceBus")]
    public class AzureServiceBusTarget : TargetWithLayout, IAsbTargetConfiguration
    {
        private const int DefaultOperationTimeoutInSeconds = 30;
        private const int DefaultMessageTimeToLiveInHours = 24 /*hours per day*/ * 14 /*days*/;
        private const int DefaultMaximumBytesPerMessageBatch = 262144 /*Azure Service Bus limit per batch*/ - 20000 /*overhead protection*/; 
        private const int DefaultMaximumMessageCountPerBatch = 300 /*To be protected from ACK flood which causes message oversize exception*/; 
        private readonly string _internalLogPrefix = $"{typeof(AzureServiceBusTarget).Name}:";

        /* https://docs.microsoft.com/en-us/azure/service-bus-messaging/service-bus-performance-improvements#reusing-factories-and-clients
        * Service Bus client objects, such as QueueClient or MessageSender, are created through a MessagingFactory object,
        * which also provides internal management of connections.
        *
        * You can safely use these client objects for concurrent asynchronous operations and from multiple threads.
        * */
        private IMessageSender _sbClient;

        /// <summary>
        /// Message mapper which transforms NLog messages to Service Bus message format
        /// </summary>
        private ILogEventToMessageMapper _mapper;

        /// <summary>
        /// Messages portion former is used in order not to sent more messages per one batch than allowed by connection
        /// </summary>
        private IMessagesPortionFormer _portionFormer;

        [RequiredParameter]
        public string ConnectionString { get; set; }

        [RequiredParameter]
        public string Queue { get; set; }

        public RetryPolicyType RetryStrategyPolicy { get; set; }

        public int ClientTimeoutInSeconds { get; set; } = DefaultOperationTimeoutInSeconds;

        public int MessageTimeToLiveInHours { get; set; } = DefaultMessageTimeToLiveInHours;

        public long MessageMaximumSizePerBatch { get; set; } = DefaultMaximumBytesPerMessageBatch;

        public int MessageMaximumCountPerBatch { get; set; } = DefaultMaximumMessageCountPerBatch;

        /// <summary>
        /// ctor default, to allow NLog to create the instance
        /// </summary>
        public AzureServiceBusTarget()
        {
        }

        /// <summary>
        /// ctor, optional. Mainly for making Unit tests possible
        /// </summary>
        /// <param name="sbClient">Message sender</param>
        /// <param name="msgMapper">Message mapper</param>
        /// <param name="portionFormer">Messages portion former</param>
        public AzureServiceBusTarget(
            IMessageSender sbClient,
            ILogEventToMessageMapper msgMapper,
            IMessagesPortionFormer portionFormer)
        {
            _sbClient = sbClient;
            _mapper = msgMapper;
            _portionFormer = portionFormer;
        }

        /// <summary>
        /// Default entry point which is called by NLog infrastructure
        /// </summary>
        protected override void InitializeTarget()
        {
            base.InitializeTarget();

            if (null != _sbClient)
            {
                InternalLogger.Info("{0} Message sender was initialized externally", _internalLogPrefix);

                if(null == _mapper)
                    throw new ArgumentNullException(nameof(_mapper), "Please make sure that message mapper is initialized");

                return;
            }

            try
            {
                InternalLogger.Trace("{0} Checking configuration parameters consistency", _internalLogPrefix);

                if (string.IsNullOrWhiteSpace(ConnectionString))
                    throw new ArgumentNullException(nameof(ConnectionString));
                if (string.IsNullOrWhiteSpace(Queue))
                    throw new ArgumentNullException(nameof(Queue));
                if (ClientTimeoutInSeconds <= 0)
                    throw new ArgumentOutOfRangeException(nameof(ClientTimeoutInSeconds));

                RetryPolicy retryPolicy;
                switch (RetryStrategyPolicy)
                {
                    case RetryPolicyType.NoRetry:
                        retryPolicy = RetryPolicy.NoRetry;
                        break;
                    case RetryPolicyType.Default:
                        retryPolicy = RetryPolicy.Default;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(retryPolicy));
                }

                InternalLogger.Trace("{0} Service Bus client initialization", _internalLogPrefix);

                _sbClient = new MessageSender(ConnectionString, Queue, retryPolicy)
                {
                    OperationTimeout = TimeSpan.FromSeconds(ClientTimeoutInSeconds)
                };

                // define mapping strategies
                _mapper = new LogEventToMessageMapper(
                    new List<IMessageMappingStrategy>
                    {
                        new MonitorDtoMapStrategy(),
                        new LogWithParamsMapStrategy()
                    },
                    TimeSpan.FromHours(MessageTimeToLiveInHours));

                _portionFormer = new MessagesPortionFormer();

                InternalLogger.Trace("{0} Service Bus client has been initialized", _internalLogPrefix);
            }
            catch (Exception)
            {
                InternalLogger.Error(
                    "{0} Invalid configuration is used for Service Bus. Initialization has been terminated.",
                    _internalLogPrefix);
                throw;
            }
        }

        protected override void Write(AsyncLogEventInfo logEvent)
        {
            try
            {
                var msg = _mapper.Map(logEvent);

                if (msg.Size > MessageMaximumSizePerBatch)
                {
                    throw new MessageIsTooBigException(msg.Size, MessageMaximumSizePerBatch);
                }

                _sbClient.SendAsync(msg).ConfigureAwait(false).GetAwaiter().GetResult(); // async doesn't work here
            }
            catch (Exception ex)
            {
                InternalLogger.Error(
                    $"{_internalLogPrefix} An error occured while sending message to Service Bus target, {ex}");

                logEvent.Continuation(ex);
            }
        }

        protected override void Write(IList<AsyncLogEventInfo> logEvents)
        {
            try
            {
                var msgSet = logEvents.Select(x => _mapper.Map(x)).ToList();

                // Split input set per portions
                var skipN = 0;
                while (true)
                {
                    var chunks = _portionFormer
                        .TakePortion(msgSet, MessageMaximumSizePerBatch, skipN)
                        .SplitIntoChunks(MessageMaximumCountPerBatch)
                        .ToList();
                    if (!chunks.Any())
                        break;

                    foreach (var chunk in chunks)
                    {
                        var chunkSet = chunk.ToList();

                        if (chunkSet.Any(x => x.Size > MessageMaximumSizePerBatch))
                        {
                            var msg = chunkSet.First(x => x.Size > MessageMaximumSizePerBatch);
                            throw new MessageIsTooBigException(msg.Size, MessageMaximumSizePerBatch);
                        }

                        // send portion to Service bus
                        _sbClient.SendAsync(chunkSet).ConfigureAwait(false)
                            .GetAwaiter().GetResult(); // async doesn't work here

                        // move to next portion
                        skipN += chunkSet.Count;
                    }
                }
            }
            catch (Exception ex)
            {
                InternalLogger.Error(
                    $"{_internalLogPrefix} An error occured while sending message to Service Bus target, {ex}");

                logEvents.First().Continuation(ex);
            }
        }

        protected override void FlushAsync(AsyncContinuation asyncContinuation)
        {
            base.FlushAsync(asyncContinuation);

            if (!_sbClient.IsClosedOrClosing)
            {
                InternalLogger.Trace(
                    "{0} Trying to close Service Bus client connection", _internalLogPrefix);

                _sbClient.CloseAsync().ConfigureAwait(false)
                    .GetAwaiter().GetResult(); // synchronous wait for the results

                InternalLogger.Trace(
                    "{0} Service Bus client connection has been closed", _internalLogPrefix);
            }
        }
    }
}
