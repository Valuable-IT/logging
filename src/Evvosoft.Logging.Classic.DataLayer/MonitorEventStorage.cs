using Evvosoft.Logging.Azure.Classic.Shared.Model.Monitor;
using Evvosoft.Logging.Classic.DataLayer.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Evvosoft.Logging.Classic.DataLayer
{
    public class MonitorEventStorage : IMonitorEventStorage, IDisposable
    {
        private const bool IsDispatched = false;
        private const string EntityName = null;
        private readonly string _sqlConnString;
        private readonly string _tableName;
        private readonly string _attTableName;
        private readonly bool _keepAlive;
        private SqlConnection _sqlConnection;

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="sqlConnString">Connection string to the DB</param>
        /// <param name="tableName">Storage table name</param>
        /// <param name="attTableName">Attachments table name</param>
        /// <param name="keepAlive">Indicates whether to keep the database connection open between the log events</param>
        public MonitorEventStorage(string sqlConnString, string tableName, string attTableName, bool keepAlive)
        {
            _sqlConnString = sqlConnString;
            _tableName = tableName;
            _attTableName = attTableName;
            _keepAlive = keepAlive;
        }

        public async Task InsertMessages(IList<MonitorMessageDto> msgSet, CancellationToken ct)
        {
            if (null == msgSet)
                throw new ArgumentNullException(nameof(msgSet));

            bool needForcedCloseConnection = false;

            try
            {
                if (_sqlConnection == null)
                {
                    _sqlConnection = new SqlConnection(_sqlConnString);
                    await _sqlConnection.OpenAsync(ct);
                }

                var tx = _sqlConnection.BeginTransaction();

                if (msgSet.Any(x => x.Attachments != null))
                {
                    // save without attachments
                    var msgWithoutAtt = msgSet.Where(x => x.Attachments == null || !x.Attachments.Any()).ToList();
                    var msgWithAtt = msgSet.Where(x => x.Attachments != null && x.Attachments.Any()).ToList();

                    await BulkCopy(msgWithoutAtt, tx, ct);
                    await WriteWithAttachments(msgWithAtt, tx, ct);
                }
                else
                {
                    // Just write all to the DB
                    await BulkCopy(msgSet, tx, ct);
                }

                tx.Commit();
            }
            catch (SqlException)
            {
                needForcedCloseConnection = true;
                throw;
            }
            catch (InvalidOperationException)
            {
                needForcedCloseConnection = true;
                throw;
            }
            finally
            {
                if (!_keepAlive || needForcedCloseConnection)
                {
                    CloseConnection();
                }
            }
        }


        /// <summary>
        /// Performs bulk copy for all messages without attachments
        /// </summary>
        /// <param name="msgSet">Messages to bulk copy</param>
        /// <param name="tx">Operation transaction</param>
        /// <param name="ct">Task cancellation token</param>
        /// <returns>Operation task</returns>
        private async Task BulkCopy(IList<MonitorMessageDto> msgSet, SqlTransaction tx, CancellationToken ct)
        {
            if (!msgSet.Any())
                return;

            using (var copy = new SqlBulkCopy(_sqlConnection, SqlBulkCopyOptions.Default, tx))
            {
                copy.DestinationTableName = _tableName;
                var table = new DataTable(_tableName);

                table.Columns.Add("MonitorID", typeof(int));
                table.Columns.Add("Name", typeof(string));
                table.Columns.Add("StackTrace", typeof(string));
                table.Columns.Add("Date", typeof(DateTime));
                table.Columns.Add("MonitorPriorityID", typeof(int));
                table.Columns.Add("MonitorTypeID", typeof(int));
                table.Columns.Add("IsDispatched", typeof(bool));
                table.Columns.Add("EntityName", typeof(string));
                table.Columns.Add("Description", typeof(string));
                table.Columns.Add("IsError", typeof(bool));
                table.Columns.Add("MonitorSourceID", typeof(int));

                foreach (var dto in msgSet)
                {
                    table.Rows.Add(
                        0,
                        dto.Message,
                        dto.StackTrace,
                        dto.Date,
                        dto.Priority,
                        dto.MonitorTypeId,
                        IsDispatched,
                        EntityName,
                        dto.Description,
                        dto.IsError ?? false,
                        dto.MonitorSourceId);
                }

                await copy.WriteToServerAsync(table, ct);
            }
        }


        /// <summary>
        /// RAW SQL single query insertion to monitor and attachments tables
        /// </summary>
        /// <param name="msgSet">Message set to be inserted</param>
        /// <param name="tx">Operation transaction</param>
        /// <param name="ct">Task cancellation token</param>
        /// <returns>Operation task</returns>
        private async Task WriteWithAttachments(IList<MonitorMessageDto> msgSet, SqlTransaction tx, CancellationToken ct)
        {
            /*
             NOTE: To improve the performance please consider two possible options:
             TableValue parameters and SP or
                INSERT INTO dbo.MyTable (ID, Name)
                SELECT 123, 'val'
                UNION ALL
                SELECT 124, 'val2'

            Pay attention that OUTPUT INSERTED.MonitorId into @pk might produce more than one identity
            when multiple rows are passed. It might be used for SP.*/

            const string attTypeKey = "@AttachmentTypeID";
            const string attachmentDataKey = "@AttachmentData";
            const string attRecordTemplate = "((SELECT ID from @pk), @AttachmentTypeID, @AttachmentData)";

            const string sqlTemplate = @"
                DECLARE @pk TABLE (ID INT);
                INSERT INTO [dbo].[##MonitorTable]
                      ([Name]
                      ,[StackTrace]
                      ,[Date]
                      ,[MonitorPriorityID]
                      ,[MonitorTypeID]
                      ,[IsDispatched]
                      ,[EntityName]
                      ,[Description]
                      ,[IsError]
                      ,[MonitorSourceID])
                OUTPUT INSERTED.MonitorId into @pk
                VALUES (@Name, @StackTrace, @Date, @MonitorPriorityID, @MonitorTypeID, @IsDispatched,
                        @EntityName, @Description, @IsError, @MonitorSourceID);
                INSERT INTO [dbo].[##MonitorAttTable]
                      ([MonitorID]
                      ,[AttachmentTypeID]
                      ,[AttachmentData])
                VALUES ##AttValues;";

            var rawSql = sqlTemplate
                .Replace("##MonitorTable", _tableName)
                .Replace("##MonitorAttTable", _attTableName);

            if (!msgSet.Any())
                return;

            foreach (var dto in msgSet)
            {
                using (var sqlCmd = new SqlCommand(rawSql, _sqlConnection, tx))
                {
                    sqlCmd.Parameters.AddWithValue("@Name", (object)dto.Message ?? DBNull.Value);
                    sqlCmd.Parameters.AddWithValue("@StackTrace", (object)dto.StackTrace ?? DBNull.Value);
                    sqlCmd.Parameters.AddWithValue("@Date", dto.Date);
                    sqlCmd.Parameters.AddWithValue("@MonitorPriorityID", (int)dto.Priority);
                    sqlCmd.Parameters.AddWithValue("@MonitorTypeID", dto.MonitorTypeId);
                    sqlCmd.Parameters.AddWithValue("@IsDispatched", IsDispatched);
                    sqlCmd.Parameters.AddWithValue("@EntityName", (object)EntityName ?? DBNull.Value);
                    sqlCmd.Parameters.AddWithValue("@Description", (object)dto.Description ?? DBNull.Value);
                    sqlCmd.Parameters.AddWithValue("@IsError", dto.IsError ?? false);
                    sqlCmd.Parameters.AddWithValue("@MonitorSourceID", dto.MonitorSourceId);

                    var attStatements = new List<string>();
                    for (var iAtt = 0; iAtt < dto.Attachments.Length; iAtt++)
                    {
                        var attType = attTypeKey + iAtt;
                        var attData = attachmentDataKey + iAtt;
                        var statement = attRecordTemplate
                            .Replace(attTypeKey, attType)
                            .Replace(attachmentDataKey, attData);
                        attStatements.Add(statement);

                        sqlCmd.Parameters.AddWithValue(attType, (int)dto.Attachments[iAtt].AttachmentType);
                        sqlCmd.Parameters.AddWithValue(attData,
                            (object)dto.Attachments[iAtt].AttachmentData ?? DBNull.Value);
                    }

                    rawSql = rawSql.Replace("##AttValues", string.Join(",", attStatements));
                    sqlCmd.CommandText = rawSql;

                    await sqlCmd.ExecuteNonQueryAsync(ct);
                }
            }
        }

        public void Dispose()
        {
            CloseConnection();
        }

        private void CloseConnection()
        {
            if (_sqlConnection != null)
            {
                _sqlConnection.Close();
                _sqlConnection = null;
            }
        }
    }
}
