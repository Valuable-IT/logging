using System.Text;
using Microsoft.Azure.ServiceBus;

namespace Evvosoft.Logging.Azure.NLog.ServiceBus.Monitor.Messages
{
    public static class MessageBuilder
    {
        private const string MsgContentType = "application/json";

        public static Message Build(string payload)
        {
            return new Message(Encoding.UTF8.GetBytes(payload))
            {
                ContentType = MsgContentType
            };
        }
    }
}
