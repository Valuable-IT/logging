namespace Evvosoft.Logging.Azure.Classic.Shared.Model.Monitor.Priority
{
    public enum MonitorPriorityType
    {
        Undefined = 0,
        Blocker = 1,
        Major = 2,
        Medium = 3,
        Low = 4
    }
}
