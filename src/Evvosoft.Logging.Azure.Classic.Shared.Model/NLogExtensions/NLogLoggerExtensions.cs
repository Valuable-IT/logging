using System;
using Evvosoft.Logging.Azure.Classic.Shared.Model.Mapping;
using Evvosoft.Logging.Azure.Classic.Shared.Model.Monitor;
using NLog;

namespace Evvosoft.Logging.Azure.Classic.Shared.Model.NLogExtensions
{
    public static class NLogLoggerExtensions
    {
        private static readonly Type DefaultWrapperType = typeof(NLogLoggerExtensions);

        public static void Log(this ILogger logger, MonitorMessageDto message)
        {
            Log(logger, message, DefaultWrapperType);
        }

        public static void Log(this ILogger logger, MonitorMessageDto message, Type wrapper)
        {
            if (null == message)
                return;

            var log = new LogEventInfo(LogLevelMapping.MapLevel(message.Priority), logger.Name, message.ToString())
            {
                TimeStamp = message.Date,
                Parameters = new object[] {message}
            };

            // example: to use in config, ${event-properties:item=description}
            log.Properties[MonitorMessageDto.MessageLogProperty] = message.Message;
            log.Properties[MonitorMessageDto.DescriptionLogProperty] = message.Description;
            log.Properties[MonitorMessageDto.StackTraceLogProperty] = message.StackTrace;
            log.Properties[MonitorMessageDto.DateLogProperty] = message.Date;
            log.Properties[MonitorMessageDto.PriorityLogProperty] = message.Priority;
            log.Properties[MonitorMessageDto.TypeIdLogProperty] = message.MonitorTypeId;
            log.Properties[MonitorMessageDto.SourceIdLogProperty] = message.MonitorSourceId;
            log.Properties[MonitorMessageDto.IsErrorLogProperty] = message.IsError;

            logger.Log(wrapper, log);
        }
    }
}
