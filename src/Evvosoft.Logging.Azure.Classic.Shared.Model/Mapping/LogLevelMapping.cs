using System;
using Evvosoft.Logging.Azure.Classic.Shared.Model.Monitor.Priority;
using NLog;

namespace Evvosoft.Logging.Azure.Classic.Shared.Model.Mapping
{
    public static class LogLevelMapping
    {
        /// <summary>
        /// Maps Monitor priority to NLog log level
        /// </summary>
        /// <param name="priority">Monitor priority level</param>
        /// <returns>NLog log level</returns>
        public static LogLevel MapLevel(MonitorPriorityType priority)
        {
            switch (priority)
            {
                case MonitorPriorityType.Undefined:
                    return LogLevel.Off;
                case MonitorPriorityType.Blocker:
                    return LogLevel.Fatal;
                case MonitorPriorityType.Major:
                    return LogLevel.Error;
                case MonitorPriorityType.Medium:
                    return LogLevel.Warn;
                case MonitorPriorityType.Low:
                    return LogLevel.Info;
                default:
                    throw new ArgumentOutOfRangeException(nameof(priority), priority,
                        "Monitor priority can't be mapped to NLog LogLevel");
            }
        }

        /// <summary>
        /// Maps NLog log level to Monitor priority
        /// </summary>
        /// <param name="level">NLog log level</param>
        /// <returns>Monitor priority level</returns>
        public static MonitorPriorityType MapLevel(LogLevel level)
        {
            if (level == LogLevel.Off)
                return MonitorPriorityType.Undefined;

            if (level == LogLevel.Fatal)
                return MonitorPriorityType.Blocker;

            if (level == LogLevel.Error)
                return MonitorPriorityType.Major;

            if (level == LogLevel.Warn)
                return MonitorPriorityType.Medium;

            if (level == LogLevel.Info)
                return MonitorPriorityType.Low;

            if (level == LogLevel.Debug)
                return MonitorPriorityType.Low;

            if (level == LogLevel.Trace)
                return MonitorPriorityType.Low;

            // Impossible case since LogLevel is class
            throw new ArgumentOutOfRangeException(nameof(level), level, "LogLevel can't be mapped to Monitor priority");
        }
    }
}
