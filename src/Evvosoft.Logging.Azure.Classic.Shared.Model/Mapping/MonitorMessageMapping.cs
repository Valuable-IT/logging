using Evvosoft.Logging.Azure.Classic.Shared.Model.Monitor;
using NLog.Common;
using NLog.LayoutRenderers;
using System;
using System.Linq;

namespace Evvosoft.Logging.Azure.Classic.Shared.Model.Mapping
{
    public static class MonitorMessageMapping
    {
        private const string ExceptionFormatStr = @"toString";

        private const int MonitorSourceId = 1;
        private const int MonitorTypeId = 1;

        public static MonitorMessageDto Map(AsyncLogEventInfo logInfo)
        {
            if (logInfo == null)
                throw new ArgumentNullException(nameof(logInfo));

            MonitorMessageDto message;
            var parameters = logInfo.LogEvent?.Parameters;
            if (parameters != null && parameters.Length > 0 && parameters.First() is MonitorMessageDto)
            {
                message = parameters.First() as MonitorMessageDto;
            }
            else
            {
                if (logInfo.LogEvent == null)
                {
                    throw new ArgumentException(nameof(logInfo.LogEvent));
                }

                var lEv = logInfo.LogEvent;

                string details = null;

                if (null != lEv.Exception)
                {
                    var exRender = new ExceptionLayoutRenderer { Format = ExceptionFormatStr };
                    details = exRender.Render(lEv);
                }

                message = new MonitorMessageDto
                {
                    Message = lEv.FormattedMessage,
                    Description = details,
                    StackTrace = lEv.Exception?.StackTrace,
                    Date = lEv.TimeStamp,
                    Priority = LogLevelMapping.MapLevel(lEv.Level),
                    MonitorSourceId = MonitorSourceId,
                    MonitorTypeId = MonitorTypeId
                };
            }

            return message;
        }
    }
}
