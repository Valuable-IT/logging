namespace Evvosoft.Logging.Azure.Classic.Shared.Model.Logging
{
    public enum MonitorPriorityType
    {
        Undefined = 0,
        Blocker = 1,
        Major = 2,
        Medium = 3,
        Low = 4
    }
}
