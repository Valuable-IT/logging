using System;
using System.Diagnostics;

namespace Evvosoft.Logging.Azure.Classic.Shared.Model.Logging.Factory
{
    public static class LogFactory
    {
        private const string ExceptionText =
            "LogFactory is not initialized. Check your code and call LogFactory.Init() before creating an ILog instance.";

        private static Func<Type, IEventLogger> _typedLogFunc;

        private static Func<string, IEventLogger> _namedLogFunc;

        private static bool _isInited;

        public static void Init(Func<Type, IEventLogger> typedLogFunc, Func<string, IEventLogger> namedLogFunc)
        {
            if (_isInited)
            {
                return;
            }

            _typedLogFunc = typedLogFunc;
            _namedLogFunc = namedLogFunc;

            _isInited = true;
        }

        public static IEventLogger CreateForCurrentClass()
        {
            if (!_isInited)
            {
                throw new InvalidOperationException(ExceptionText);
            }

            StackTrace stackTrace = new StackTrace();

            var callingMethod = stackTrace.GetFrame(1).GetMethod();

            var callingType = callingMethod.DeclaringType;

            return _typedLogFunc(callingType);
        }

        public static IEventLogger CreateWithName(string name)
        {
            if (!_isInited)
            {
                throw new InvalidOperationException(ExceptionText);
            }

            return _namedLogFunc(name);
        }

        public static IEventLogger CreateWithType(Type loggerType)
        {
            if (!_isInited)
            {
                throw new InvalidOperationException(ExceptionText);
            }

            return _typedLogFunc(loggerType);
        }

        public static IEventLogger LoggerFor<T>() where T : class => CreateWithType(typeof(T));
    }
}
