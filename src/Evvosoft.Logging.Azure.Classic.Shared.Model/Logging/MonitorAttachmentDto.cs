using Newtonsoft.Json;

namespace Evvosoft.Logging.Azure.Classic.Shared.Model.Logging
{
    public class MonitorAttachmentDto
    {
        /// <summary>
        /// Attachment payload
        /// </summary>
        [JsonProperty(PropertyName = "ad")]
        public string AttachmentData { get; set; }

        /// <summary>
        /// Attachment payload type
        /// </summary>
        [JsonProperty(PropertyName = "at")]
        public AttachmentTypeDto AttachmentType { get; set; }
    }
}
