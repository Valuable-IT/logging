using System;
using System.Threading.Tasks;
using Evvosoft.Logging.Azure.Shared.Model.Logging;
using Evvosoft.Logging.Azure.Shared.Model.NLogExtensions;

namespace NLog.Console
{
    class Program
    {
        private static readonly ILogger Logger = LogManager.GetCurrentClassLogger();

        static async Task Main()
        {
            var n = 1000;

            while (n-->0)
            {
                var monitorMessage = new MonitorMessageDto
                {
                    Date = DateTime.Now,
                    Message = "Test" + n,
                    Description = "Test Description",
                    MonitorSource = MonitorSource.Central,
                    MonitorType = MonitorType.General,
                    Priority = MonitorPriority.Low,
                    StackTrace = "StackTrace",
                };

                Logger.Log(monitorMessage,true);
                Logger.Debug("test debug message");
            }
    
            var task1 = Task.Factory.StartNew(Do);
            var task2 = Task.Factory.StartNew(Do);
            var task3 = Task.Factory.StartNew(Do);
            var task4 = Task.Factory.StartNew(Do);
            var task5 = Task.Factory.StartNew(Do);
            var task6 = Task.Factory.StartNew(Do);
            var task7 = Task.Factory.StartNew(Do);
            var task8 = Task.Factory.StartNew(Do);

            await Task.WhenAll(task1, task2, task3, task4, task5, task6, task7, task8);

            LogManager.Flush(TimeSpan.FromMinutes(10));
        }

        private static void Do()
        {
            ILogger localLog = LogManager.GetCurrentClassLogger();

            int nCnt = 500;
            while (nCnt-- > 0)
            {
                localLog.Trace($"Trace {nCnt} at {System.Threading.Thread.CurrentThread.ManagedThreadId}");
                localLog.Debug($"Debug {nCnt} at {System.Threading.Thread.CurrentThread.ManagedThreadId}");
                localLog.Warn($"Warn {nCnt} at {System.Threading.Thread.CurrentThread.ManagedThreadId}");
            }
        }
    }
}
